#pragma once

#include "DynamicEntity.h"
#include <SFML\Window.hpp>

class FirstPersonCamera;
class PhysicsWorld;
class HUD;

class CharacterEntity :
	public DynamicEntity {

friend class FirstPersonCamera;
friend class HUD;

public:
	CharacterEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1, btVector3 &viewPosition = btVector3(0, 0, 0), btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~CharacterEntity();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);

	btVector3 GetViewPosition();
	btVector3 GetViewDirection();

protected:
	float speed;
	float shotPower;
	float fireRate;
	float totalAngleY, totalAngleX;
	btVector3 viewDirection, viewPosition;
	sf::Clock clock;

};

