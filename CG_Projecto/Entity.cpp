#include "stdafx.h"
#include "Entity.h"

#include "Renderer.h"
#include "PhysicsWorld.h"

AssetsManager Entity::assetsManager = AssetsManager();

Entity::Entity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btVector3 &scale) {
	dynamicsWorld = world;
	renderer = assetsManager.GetRenderer(name);
	shape = assetsManager.GetShape(name, scale);

	std::vector<shared_ptr<sf::SoundBuffer>> buffers = assetsManager.GetSounds(name);
	for (int i = 0; i < buffers.size(); ++i) {
		sounds.push_back(sf::Sound(*buffers[i].get()));
	}

	collisionCallback = false;
	readyToDie = false;
	rendererMeshID = -1;
}

Entity::Entity(	shared_ptr<PhysicsWorld> world,
				shared_ptr<Renderer> renderer,
				unsigned int rendererMeshId,
				shared_ptr<btMotionState> motionState,
				unique_ptr<btRigidBody> rigidBody,
				std::vector<sf::Sound> sounds)
{
	dynamicsWorld = world;
	this->renderer = renderer;	
	this->rendererMeshID = rendererMeshId;
	this->motionState = motionState;
	this->rigidBody = std::move(rigidBody);
	this->sounds = sounds;

	if (this->rigidBody) {
		this->rigidBody->setUserPointer(this);
		dynamicsWorld->GetWorld()->addRigidBody(this->rigidBody.get());
	}
	
	collisionCallback = false;
	readyToDie = false;
}

Entity::~Entity() {}

void Entity::GenerateRigidBody(btTransform &transform, btScalar mass) {
	if (!shape) return;

	btVector3 inertia(0, 0, 0);
	if (mass > 0)
		shape->calculateLocalInertia(mass, inertia);

	motionState = std::make_shared<btDefaultMotionState>(transform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState.get(), shape.get(), inertia);
	rigidBody = std::make_unique<btRigidBody>(rbInfo);
	rigidBody->setUserPointer(this);
	dynamicsWorld->GetWorld()->addRigidBody(rigidBody.get());
}

weak_ptr<btMotionState> Entity::GetMotionState() {
	return weak_ptr<btMotionState>(motionState);
}

btVector3 Entity::GetPosition() {
	if (motionState == nullptr) return btVector3(0, 0, 0);
	btTransform t;
	motionState->getWorldTransform(t);
	return t.getOrigin();
}

btQuaternion Entity::GetRotation() {
	if (motionState == nullptr) return btQuaternion(0, 0, 0);
	btTransform t;
	motionState->getWorldTransform(t);
	return t.getRotation();
}

btVector3 Entity::GetForwardVector() {
	if (motionState == nullptr) return btVector3(0, 0, 1);
	btTransform t;
	motionState->getWorldTransform(t);
	return t.getBasis().getColumn(2);
}

btRigidBody *Entity::GetRigidBody() {
	return rigidBody.get();
}

void Entity::RemoveFromWorld() {
	if (!rigidBody.get())
		LOG(TLogLevel::logDEBUG) << "Null: " << GetDescription();
	else
		dynamicsWorld->GetWorld()->removeRigidBody(rigidBody.get());
	readyToDie = true;
}

bool Entity::HasCollisionCallback() {
	return collisionCallback;
}

bool Entity::IsReadyToDie() {
	if (!readyToDie) return false;

	for each (sf::Sound s in sounds) {
		if (s.getStatus() != sf::Sound::Stopped) return false;
	}

	return true;
}

void Entity::CleanAssets() {
	assetsManager.CleanAssets();
}