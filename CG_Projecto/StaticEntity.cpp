#include "stdafx.h"
#include "StaticEntity.h"

#include "Renderer.h"
#include "PhysicsWorld.h"

StaticEntity::StaticEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btTransform &transform, btVector3 &scale)
	:Entity(world, name, scale)
{
	GenerateRigidBody(transform);
}

StaticEntity::StaticEntity(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds)
	: Entity(world, renderer, rendererMeshId, motionState, std::move(rigidBody), sounds)
{}

StaticEntity::~StaticEntity() { }

void StaticEntity::Draw(float deltaTime, bool lighting, bool cullface) {
	if (rigidBody == nullptr) return;

	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	const btVector3 scale = rigidBody->getCollisionShape()->getLocalScaling();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glPushMatrix();
		glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
		glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
		glScalef(scale.x(), scale.y(), scale.z());

		if (rendererMeshID < 0)	renderer->Draw(lighting, cullface);
		else					renderer->Draw(rendererMeshID, lighting, cullface);
	glPopMatrix();
	glDisable(GL_NORMALIZE);
}

void StaticEntity::Update(float deltaTime) {

}