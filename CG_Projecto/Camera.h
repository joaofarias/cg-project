#pragma once

class HUD;

class Camera {

public:
	Camera();
	Camera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar);
	Camera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar, btVector3 &pos, btVector3 &target, btVector3 &up);
	~Camera();

	virtual void Update(float deltaTime);
	void PrepareDrawing();

	void SetPerspective(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar);
	void SetFOV(GLdouble fov);
	void SetRatio(GLdouble ratio);
	void SetZView(GLdouble zNear, GLdouble zFar);

	void SetView(btVector3 &pos, btVector3 &target, btVector3 &up);
	void SetPosition(btVector3 &pos);
	void SetTarget(btVector3 &target);
	void SetUpVector(btVector3 &up);

	btVector3 GetPosition();

	void SetHUD(unique_ptr<HUD> hud);
	virtual void DrawHUD(float deltaTime);
	virtual void OnLoadingStart();
	virtual void OnLoadingEnd();

protected:
	GLdouble fov;
	GLdouble ratio;
	GLdouble zNear;
	GLdouble zFar;
	btVector3 viewProperties[3];

	unique_ptr<HUD> hud;

};

