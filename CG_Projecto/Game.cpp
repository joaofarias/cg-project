#include "stdafx.h"
#include "Game.h"

#include "PhysicsWorld.h"
#include "FirstPersonCamera.h"
#include "DalekHUD.h"
#include "Dalek.h"
#include "Light.h"
#include "StaticEntity.h"
#include "BreakableEntity.h"
#include "FirstRoom.h"
#include "SecondRoom.h"

int main()
{
	Game game = Game();
	game.RunGame();

	return 0;
}

int Game::SCREEN_WIDTH;
int Game::SCREEN_HEIGHT;
std::list<const sf::Event> Game::events;
std::list<const sf::Text> Game::strings;

Game::Game() {
	gameRunning = false;
	
	// sets window's properties
	videoMode = sf::VideoMode::getDesktopMode();
	if (!videoMode.isValid()) {
		std::vector<sf::VideoMode> supportedVideoModes = sf::VideoMode::getFullscreenModes();
		for (std::size_t i = 0; i < supportedVideoModes.size(); ++i) {
			if (supportedVideoModes[i].isValid()) {
				videoMode = supportedVideoModes[i];
				break;
			}

		}
	}

	sf::ContextSettings settings = sf::ContextSettings(24, 8);
	window.create(videoMode, "", sf::Style::Fullscreen, settings);
	window.setVerticalSyncEnabled(true);
	window.setMouseCursorVisible(false);
	LOG(TLogLevel::logDEBUG) << "Stencil bits: " << window.getSettings().stencilBits;
	LOG(TLogLevel::logDEBUG) << "Depth bits: " << window.getSettings().depthBits;

	SCREEN_WIDTH = window.getSize().x;
	SCREEN_HEIGHT = window.getSize().y;
	
	sf::Mouse::setPosition(sf::Vector2i(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));

	// initializes opengl states
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculation
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// initializes game's components
	physicsWorld = std::make_unique<PhysicsWorld>();
	physicsWorld->AddLevel(std::make_unique<FirstRoom>(physicsWorld));
	physicsWorld->AddLevel(std::make_unique<SecondRoom>(physicsWorld));
}

Game::~Game() {}

void Game::RunGame() {
	float deltaTime;
	float gamePaused = false;

	gameRunning = true;
	sf::Clock clock;
	gameClock.restart();
	while (gameRunning)
	{
		events.clear();
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::KeyReleased) {
				if (event.key.code == sf::Keyboard::Escape)
					gameRunning = false;
			}
			else if (event.type == sf::Event::LostFocus) {
				gamePaused = true;
			}
			else if (event.type == sf::Event::GainedFocus) {
				gamePaused = false;
			}
			events.push_back(sf::Event(event));
		}

		deltaTime = clock.restart().asSeconds();

		if (!gamePaused) {
			Update(deltaTime);

			Draw(deltaTime);

			window.display();
		}
		
	}

	window.close();
}

void Game::Draw(float deltaTime) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
		
	physicsWorld->DrawWorld(deltaTime);

	window.pushGLStates();	// for the text drawing

	for each (sf::Text str in strings)
		window.draw(str);
	strings.clear();

	window.popGLStates();
}

void Game::Update(float deltaTime) {
	physicsWorld->Update(deltaTime);
}

int Game::GetScreenWidth() {
	return SCREEN_WIDTH;
}
int Game::GetScreenHeight() {
	return SCREEN_HEIGHT;
}

void Game::DrawString(const sf::Text str) {
	strings.push_back(str);
}