// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <SFML\OpenGL.hpp>						// OpenGL
#include "btBulletDynamicsCommon.h"				// Physics Engine
#include "LinearMath\btConvexHullComputer.h"	// Convex Hull Generator
#include "hacdHACD.h"							// Convex Hull Decomposer
#include "BulletCollision\CollisionShapes\btShapeHull.h"
#include "btBulletWorldImporter.h"				// Shape importer
#include "assimp\Importer.hpp"					// Model importer
#include "assimp\scene.h"
#include "assimp\postprocess.h"					
#include "Poco\File.h"							// Filesystem library
#include "Poco\Path.h"
#include "Poco\DOM\DOMParser.h"					// XML library
#include "Poco\DOM\Document.h"
#include "Poco\DOM\TreeWalker.h"
#include "Poco\DOM\NodeIterator.h"
#include "Poco\DOM\NodeFilter.h"
#include "Poco\DOM\NamedNodeMap.h"
#include "Poco\DOM\NodeList.h"
#include "Poco\DOM\AutoPtr.h"
#include "Poco\DOM\DOMWriter.h"
#include "Poco\XML\XMLWriter.h"
#include "Poco\SAX\InputSource.h"
#include <vector>	
#include <forward_list>
#include <list>
#include <unordered_map>
#include <memory>								// Smart pointers
#include <iostream>
#include <functional>

#include "Log.h"								// Log system
#include "Colors.h"								// Color list -- CHECK: Is this really needed??

#define RAD2DEG(rad) rad*180.f/AI_MATH_PI_F
#define DEG2RAD(deg) deg/180.f*AI_MATH_PI_F

using std::unique_ptr;
using std::shared_ptr;
using std::weak_ptr;
using std::string;