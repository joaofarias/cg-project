#include "stdafx.h"
#include "CharacterEntity.h"

#include "Game.h"
#include "Renderer.h"
#include "PhysicsWorld.h"

CharacterEntity::CharacterEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btVector3 &viewPosition, btTransform &transform, btVector3 &scale)
	: DynamicEntity(world, name, mass, transform, scale) {
	rigidBody->setAngularFactor(0);
	rigidBody->setFriction(1);
	rigidBody->setDamping(0.9f, 0.9f);
	rigidBody->setActivationState(DISABLE_DEACTIVATION);
	speed = 150;
	shotPower = 100000;
	fireRate = 0.5f;
	this->viewPosition = viewPosition;
	totalAngleY = RAD2DEG(btVector3(1, 0, 0).angle(transform.getBasis().getColumn(0)));
	totalAngleX = 0;
	
	clock.restart();
}

CharacterEntity::~CharacterEntity() {}

void CharacterEntity::Draw(float deltaTime, bool lighting, bool cullface) {
	if (rigidBody == nullptr) return;

	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	const btVector3 scale = rigidBody->getCollisionShape()->getLocalScaling();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glPushMatrix();
	glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
	glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
	glScalef(scale.x(), scale.y(), scale.z());

	renderer->Draw(lighting, cullface);
	glPopMatrix();
	glDisable(GL_NORMALIZE);
}

void CharacterEntity::Update(float deltaTime) {
	btTransform transform;
	motionState->getWorldTransform(transform);

	btVector3 dir = transform.getBasis().getColumn(0).normalized();
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		rigidBody->applyCentralImpulse(dir*speed * deltaTime);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		rigidBody->applyCentralImpulse(-dir*speed * deltaTime);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(90)).normalized();
		rigidBody->applyCentralImpulse(dir*speed * deltaTime);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(-90)).normalized();
		rigidBody->applyCentralImpulse(dir*speed * deltaTime);
	}
	dir = transform.getBasis().getColumn(0).normalized();

	int mouseX = sf::Mouse::getPosition().x;
	int mouseY = sf::Mouse::getPosition().y;

	totalAngleY -= (mouseX - Game::GetScreenWidth()/2) * 4 * deltaTime;
	if (totalAngleY > 360.f) totalAngleY -= 360;
	else if(totalAngleY < -360.f) totalAngleY += 360.f;

	totalAngleX += (Game::GetScreenHeight() / 2 - mouseY) * 4 * deltaTime;
	if (totalAngleX > 90.f) totalAngleX = 90.f;
	else if (totalAngleX < -90.f) totalAngleX = -90.f;

	viewDirection = btVector3(0, 0, 0);
	viewDirection.setY(sinf(DEG2RAD(totalAngleX))*5.f);
	viewDirection += dir;
	viewDirection.normalize();

	rigidBody->getMotionState()->getWorldTransform(transform);
	transform.setRotation(btQuaternion(DEG2RAD(totalAngleY), 0, 0));
	rigidBody->getMotionState()->setWorldTransform(transform);
	rigidBody->setCenterOfMassTransform(transform);

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && clock.getElapsedTime().asSeconds() > fireRate) {
		btTransform projTransf;
		projTransf.setIdentity();
		projTransf.setOrigin(transform.getOrigin() + viewPosition + viewDirection);
		unique_ptr<DynamicEntity> projectile = std::make_unique<DynamicEntity>(dynamicsWorld, AssetsManager::AssetName::Sphere, 3000.f, projTransf, btVector3(0.25f, 0.25f, 0.25f));
		projectile->ApplyCentralImpulse(viewDirection * shotPower);
		dynamicsWorld->AddEntity(std::move(projectile));		
		clock.restart();
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) && clock.getElapsedTime().asSeconds() > fireRate) {
		rigidBody->setGravity(-rigidBody->getGravity());
		rigidBody->applyGravity();
		clock.restart();
	}
}

btVector3 CharacterEntity::GetViewPosition() {
	return viewPosition;
}

btVector3 CharacterEntity::GetViewDirection() {
	return viewDirection;
}