#include "stdafx.h"
#include "Target.h"

Target::Target(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
: BreakableEntity(world, name, mass, transform, scale)
{
	for (int i = 0; i < rigidBodies.size(); ++i) {
		rigidBodies[i]->setRollingFriction(1);
		rigidBodies[i]->setFriction(1);
		rigidBodies[i]->setDamping(0.1f, 0.75f);
		rigidBodies[i]->setSleepingThresholds(0.2f, 0.2f);
	}
	
	checkStructure = false;
	hit = false;
	canPlaySound = false;
}

Target::~Target() {}

bool Target::WasHit() {
	return hit;
}

void Target::Update(float deltaTime) {
	if (canPlaySound && sounds.size() > 0) {
		for (int i = 0; i < sounds.size(); ++i) {
			if (sounds[i].getStatus() != sf::Sound::Playing) {
				sounds[i].play();
				canPlaySound = false;
			}
		}
		if (canPlaySound) {
			sounds.push_back(sf::Sound(*((sf::SoundBuffer*)sounds[0].getBuffer())));
			sounds.back().play();
			canPlaySound = false;
		}
	}

	BreakableEntity::Update(deltaTime);
	canPlaySound = false;
}

void Target::OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint) {
	if (hit) return;

	Target *target = dynamic_cast<Target *>((Entity *) exploder->getUserPointer());
	if (target == this) {
		hit = true;

		for (int i = 0; i < rigidBodies.size(); ++i) {
			ToDynamic(rigidBodies[i].get());
		}

		checkStructure = true;

		canPlaySound = true;
	}
}