#include "stdafx.h"
#include "Trigger.h"

#include "PhysicsWorld.h"


Trigger::Trigger(shared_ptr<PhysicsWorld> world, unique_ptr<btCollisionShape> shape, btTransform &trans)
{
	this->world = world;
	this->shape = std::move(shape);
	trigger = std::make_unique<btCollisionObject>();
	trigger->setCollisionFlags(trigger->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	trigger->setCollisionShape(this->shape.get());
	trigger->setWorldTransform(trans);
}

Trigger::~Trigger() {}

bool Trigger::TriggeredBy(btCollisionObject *obj) {
	LevelTriggerTest triggerResult;
	world->GetWorld()->contactPairTest(trigger.get(), obj, triggerResult);
	if (triggerResult.trigger)
		return true;
	return false;
}

btScalar Trigger::LevelTriggerTest::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1) {
	if (cp.getDistance() < 0.001f)
		trigger = true;
	return 0.f;
}