#pragma once

class Entity;
class CharacterEntity;
class BulletDebugRenderer;
class Camera;
class Light;
class Level;
class Trigger;

class PhysicsWorld
{
public:
	PhysicsWorld();
	~PhysicsWorld();

	void DrawWorld(float deltaTime);
	void Update(float deltaTime);
	void SetPlayer(shared_ptr<CharacterEntity> player);
	void AddEntity(shared_ptr<Entity> entity, bool transparent = false, bool reflective = false);
	void SetCamera(shared_ptr<Camera> cam);
	void AddLight(shared_ptr<Light> light);
	void AddLevel(shared_ptr<Level> level);
	void SetLevelTrigger(unique_ptr<Trigger> trigger);
	btDiscreteDynamicsWorld *GetWorld();
	weak_ptr<btMotionState> GetPlayerMotionState();
	weak_ptr<CharacterEntity> GetPlayer();
	int GetNumActiveObjects();
	void DebugDraw();

	void OnSimulationTick(btScalar timeStep);

private:
	unique_ptr<btDefaultCollisionConfiguration> collisionCfg;
	unique_ptr<btCollisionDispatcher> collisionDispatcher;
	unique_ptr<btBroadphaseInterface> overlappingPairCache;
	unique_ptr<btSequentialImpulseConstraintSolver> solver;
	unique_ptr<BulletDebugRenderer> debugDrawer;
	unique_ptr<btDiscreteDynamicsWorld> dynamicsWorld;

	struct TransparentEntityItem {
		shared_ptr<Entity> entity;
		float distanceToCamera;
		bool isReflective;

		TransparentEntityItem(shared_ptr<Entity> ent, bool isReflective);
	};

	std::list<shared_ptr<Entity>> entities;
	std::list<unique_ptr<TransparentEntityItem>> transparentEntities;
	shared_ptr<CharacterEntity> player;
	shared_ptr<Camera> camera;
	std::vector<shared_ptr<Light>> lights;
	std::vector<shared_ptr<Level>> levels;
	unique_ptr<Trigger> levelTrigger;

	bool loading;
	int currentLevel;
	int currentStencilBit;
	int numActiveObjects;

	void DrawReflection(Entity *reflector, float deltaTime, int n);
	void DrawReflectedEntity(Entity *reflector, Entity *reflected, float deltaTime);
	void RemoveEntities();
	void NextLevel();

	inline static bool comp(const unique_ptr<TransparentEntityItem> &a, const unique_ptr<TransparentEntityItem> &b) { return a->distanceToCamera > b->distanceToCamera; }
};

