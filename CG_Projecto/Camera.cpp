#include "stdafx.h"
#include "Camera.h"

#include "SFML\Window.hpp"
#include "HUD.h"

Camera::Camera()
	: Camera(90, 1920.0 / 1080.0, 0.1, 1000) {
}

Camera::Camera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar)
	: Camera(fov, ratio, zNear, zFar, btVector3(0, 0, 0), btVector3(0, 0, -1), btVector3(0, 1, 0)) {
}

Camera::Camera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar, btVector3 &pos, btVector3 &target, btVector3 &up) {
	this->fov = fov;
	this->ratio = ratio;
	this->zNear = zNear;
	this->zFar = zFar;
	
	viewProperties[0] = pos;
	viewProperties[1] = target;
	viewProperties[2] = up;
}

Camera::~Camera() {}

void Camera::Update(float deltaTime) {
	if (hud != nullptr)
		hud->Update(deltaTime);
}

void Camera::PrepareDrawing() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fov, ratio, zNear, zFar);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(viewProperties[0].x(), viewProperties[0].y(), viewProperties[0].z(),
		viewProperties[1].x(), viewProperties[1].y(), viewProperties[1].z(),
		viewProperties[2].x(), viewProperties[2].y(), viewProperties[2].z());
}

void Camera::SetPerspective(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar) {
	SetFOV(fov);
	SetRatio(ratio);
	SetZView(zNear, zFar);
}

void Camera::SetFOV(GLdouble fov) {
	this->fov = fov;
}

void Camera::SetRatio(GLdouble ratio) {
	if (ratio <= 0) return;
	this->ratio = ratio;
}

void Camera::SetZView(GLdouble zNear, GLdouble zFar) {
	if ((zNear <= 0 && zFar <= 0) || (zFar <= zNear )) return;
	else if (zNear <= 0) this->zFar = zFar;
	else if (zFar <= 0) this->zNear = zNear;
	else {
		this->zNear = zNear;
		this->zFar = zFar;
	}
}

void Camera::SetView(btVector3 &pos, btVector3 &target, btVector3 &up) {
	SetPosition(pos);
	SetTarget(target);
	SetUpVector(up);
}

void Camera::SetPosition(btVector3 &pos) {
	viewProperties[0] = pos;
}

void Camera::SetTarget(btVector3 &target) {
	viewProperties[1] = target;
}

void Camera::SetUpVector(btVector3 &up) {
	viewProperties[2] = up;
}

btVector3 Camera::GetPosition() {
	return viewProperties[0];
}

void Camera::SetHUD(unique_ptr<HUD> hud) {
	this->hud = std::move(hud);
}

void Camera::DrawHUD(float deltaTime) {
	if (hud != nullptr)
		hud->Draw(deltaTime);
}

void Camera::OnLoadingStart() {
	if (hud != nullptr)
		hud->OnLoadingStart();
}

void Camera::OnLoadingEnd() {
	if (hud != nullptr)
		hud->OnLoadingEnd();
}