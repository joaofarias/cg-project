#include "stdafx.h"
#include "HUD.h"

#include "Game.h"
#include "IL\il.h"
#include "Poco\Exception.h"

using namespace Poco::XML;

HUD::HUD(weak_ptr<CharacterEntity> player, string configFileName)
{
	this->player = player;

	Poco::Path filePath = Poco::Path(ASSETS_FILE_DIRECTORY);

	Poco::File file = Poco::File(filePath);
	if (!file.exists()) {}
	file.createDirectories();

	filePath.setFileName(configFileName);
	file = filePath;
	if (!file.exists())
		LOG(TLogLevel::logWARNING) << "A HUD was being loaded but no configuration file was found.";
	else {
		DOMParser parser;
		try {
			hudConfig = parser.parse(file.path());
			LoadTextures();
			LoadFonts();
		} catch (Poco::Exception &exc) { LOG(TLogLevel::logERROR) << exc.displayText(); }
	}
		
}

HUD::~HUD() {}

void HUD::Draw(float deltaTime) {
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	
	glColor4f(0.f, 0.f, 0.f, 0.f);
	for (int i = 0; i < textures.size(); i++) {
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		
		glBegin(GL_POLYGON);
			glTexCoord2f(0.f, 0.f);	glVertex3f(0.f, 0.f, (GLfloat) i);
			glTexCoord2f(0.f, 1.f);	glVertex3f(0.f, (GLfloat) Game::GetScreenHeight(), (GLfloat) i);
			glTexCoord2f(1.f, 1.f);	glVertex3f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight(), (GLfloat) i);
			glTexCoord2f(1.f, 0.f);	glVertex3f((GLfloat) Game::GetScreenWidth(), 0.f, (GLfloat) i);
		glEnd();
	}

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glDisable(GL_BLEND);
}

void HUD::Update(float deltaTime) {}

void HUD::OnLoadingStart() {
	sf::Text loadingText = sf::Text("Loading...", fonts.begin()->second, 25);
	loadingText.setColor(sf::Color::White);
	loadingText.setOrigin(loadingText.getLocalBounds().width / 2.f, loadingText.getLocalBounds().height / 2.f);
	loadingText.setPosition(Game::GetScreenWidth()/2.f, Game::GetScreenHeight()/2.f);
	Game::DrawString(loadingText);
}

void HUD::OnLoadingEnd() {}

void HUD::LoadTextures() {
	NodeList *nodeList = hudConfig->getElementsByTagName(TEXTURE_TAG_NAME);
	if (!nodeList || nodeList->length() == 0) {
		nodeList->release();
		return;
	}

	/* Before calling ilInit() version should be checked. */
	if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
	{
		ILint test = ilGetInteger(IL_VERSION_NUM);
		/// wrong DevIL version ///
		LOG(TLogLevel::logERROR) << "Wrong DevIL version. Old devil.dll in system32/SysWow64?";
		return;
	}

	ilInit(); /* Initialization of DevIL */

	int numTextures = nodeList->length();
	textures.resize(numTextures);

	/* array with DevIL image IDs */
	ILuint* imageIds = NULL;
	imageIds = new ILuint[numTextures];

	/* generate DevIL Image IDs */
	ilGenImages(numTextures, imageIds); /* Generation of numTextures image names */

	/* create and fill array with GL texture ids */
	glGenTextures(numTextures, &textures[0]); /* Texture name generation */
	
	for (int i = 0; i < numTextures; i++) {
		string texFilename = ((Element *) nodeList->item(i))->getAttribute("path");

		ilBindImage(imageIds[i]); /* Binding of DevIL image name */
		ILboolean success = ilLoadImage((wchar_t*) texFilename.c_str());

		if (success) /* If no error occured: */
		{
			success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); /* Convert every colour component into
																 unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
			if (!success)
			{
				/* Error occured */
				LOG(TLogLevel::logERROR) << "Couldn't convert image";
				continue;
			}
			
			glBindTexture(GL_TEXTURE_2D, textures[i]); /* Binding of texture name */
			//redefine standard texture values
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear
																			  interpolation for magnification filter */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear
																			  interpolation for minifying filter */
			glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH),
				ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
				ilGetData()); /* Texture specification */
		}
		else
		{
			/* Error occured */
			LOG(TLogLevel::logERROR) << "Couldn't load Image: " << texFilename;
		}

	}	

	ilDeleteImages(numTextures, imageIds); /* Because we have already copied image data into texture data
										   we can release memory used by image. */
	//Cleanup
	delete[] imageIds;
	imageIds = NULL;

}

void HUD::LoadFonts() {
	NodeList *nList = hudConfig->getElementsByTagName("font");
	if (!nList) return;

	for (int i = 0; i < nList->length(); ++i) {
		sf::Font textFont;
		if (!textFont.loadFromFile(((Element *) nList->item(i))->getAttribute("path"))) {
			LOG(TLogLevel::logERROR) << "Unable to load font file.";
			continue;
		}
		string name = ((Element *) nList->item(i))->getAttribute("name");
		fonts.insert(std::make_pair(name, textFont));
	}
}