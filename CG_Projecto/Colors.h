#pragma once

#define BLUE		0.0, 0.0, 1.0, 1.0
#define RED			1.0, 0.0, 0.0, 1.0
#define YELLOW		1.0, 1.0, 0.0, 1.0
#define GREEN		0.0, 1.0, 0.0, 1.0
#define ORANGE		1.0, 0.5, 0.1, 1.0
#define WHITE		1.0, 1.0, 1.0, 1.0
#define BLACK		0.0, 0.0, 0.0, 1.0
#define GRAY		0.9, 0.92, 0.29, 1.0