#pragma once
class ShapeImporter : public btWorldImporter
{
public:
	ShapeImporter();
	~ShapeImporter();

	btCollisionShape *GetShapeFromFile(const string &fileName);
	btCollisionShape *GetShapeFromMemory(char *data, int size);
};

