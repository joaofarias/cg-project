#pragma once

#include "SFML\Audio.hpp"

#define ASSETS_FILE_DIRECTORY	"..\\assets\\"
#define ASSETS_FILE_NAME		"assets.xml"
#define SHAPE_FILE_DIRECTORY	"..\\assets\\shapes\\"

class Renderer;
class ShapeImporter;

class AssetsManager
{
public:
	enum AssetName;

	AssetsManager();
	~AssetsManager();

	shared_ptr<Renderer> GetRenderer(AssetName name);
	shared_ptr<btCollisionShape> GetShape(AssetName name, btVector3 &scale);
	std::vector<shared_ptr<sf::SoundBuffer>> GetSounds(AssetName name);
	void CleanAssets();
private:
	struct ShapeKey {
		AssetName identifier;
		btVector3 scale;

		bool operator==(const ShapeKey &other) const {
			return (identifier == other.identifier && scale == other.scale);
		}
	};
	
	struct ShapeKeyHasher {
		std::size_t operator()(const ShapeKey& k) const {
			return (((std::hash<int>()(k.identifier)		  ^ (std::hash<float>()((float) k.scale.x()) << 1)) >> 1) ^
					((std::hash<float>()((float) k.scale.y()) ^ (std::hash<float>()((float) k.scale.z()) >> 1)) << 1));
		}
	};

	Poco::AutoPtr<Poco::XML::Document> doc;
	std::unordered_map<ShapeKey, shared_ptr<btCollisionShape>, ShapeKeyHasher> shapes;
	std::map<AssetName, shared_ptr<Renderer>> renderers;
	std::map<AssetName, std::vector<shared_ptr<sf::SoundBuffer>>> sounds;
	std::map<btCollisionShape*, std::vector<unique_ptr<btCollisionShape>>> compoundsChildShapes;
	static ShapeImporter shapeImporter;
	static const string assetTypes[];

	void CreateXML(const Poco::File &file);
	void LoadXML(const Poco::File &file);
	string GetModelPath(const string &id);
	bool GetShapePath(const string &id, string &pathOut);
	bool GetSoundPath(const string &id, int n, string &pathOut);
	void AddShapePath(const string &id, const string &path);
	shared_ptr<btCollisionShape> CreateShape(AssetName name, const string &primitive);
	shared_ptr<btCollisionShape> CreateConvexHull(AssetName name);
	shared_ptr<btCollisionShape> CreateBreakableCompound(AssetName name);
	shared_ptr<btCollisionShape> CreateCompoundShape(AssetName name);
	HACD::HACD *CalcHACD(const btAlignedObjectArray<btVector3> &vertices, const btAlignedObjectArray<btVector3> &triangleIndices);
	
		
	typedef std::map<AssetName, shared_ptr<Renderer>>::const_iterator RendererHashPair;
	typedef std::unordered_map<ShapeKey, shared_ptr<btCollisionShape>, ShapeKeyHasher>::const_iterator ShapeHashPair;
	typedef std::map<AssetName, std::vector<shared_ptr<sf::SoundBuffer>>>::const_iterator SoundsHashPair;
};

enum AssetsManager::AssetName {
	DalekBody,
	DalekEye,
	DalekGun,
	BlueLaser,
	Sphere,
	Cube,
	Torus,
	ChessFloor,
	Ceiling,
	Wall,
	StructureTest,
	FirstRoom,
	FirstRoomWall,
	Door,
	Mirror,
	Target,
	Glass_2x2,
	GlassBroken_2x2,
	Glass_4x2,
	GlassBroken_4x2,
	Glass_2x6,
	GlassBroken_2x6,
	Glass_2x8,
	GlassBroken_2x8,
	SecondRoom,
	SecondRoomWall
};