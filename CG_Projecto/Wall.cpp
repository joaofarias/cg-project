#include "stdafx.h"
#include "Wall.h"

#include "PhysicsWorld.h"
#include "Glass.h"
#include "BrokenGlass.h"

Wall::Wall(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
	: BreakableEntity(world, name, mass, transform, scale)
{
	for (int i = 0; i < rigidBodies.size(); ++i) {
		rigidBodies[i]->setRollingFriction(1);
		rigidBodies[i]->setFriction(1);
		rigidBodies[i]->setDamping(0.5f, 0.75f);
	}
}

Wall::~Wall() {}

bool Wall::IsAttached(btRigidBody *rb, btRigidBody *from) {
	HashPair pair = attachedRBs.find(rb);
	if (pair != attachedRBs.end())
		return pair->second;

	attachedRBs[rb] = false;

	CollisionTestResult ctResult;
	ctResult.testingObject = rb;
	dynamicsWorld->GetWorld()->contactTest(rb, ctResult);

	for (int i = 0; i < ctResult.collidedObjects.size(); ++i) {
		if (!ctResult.collidedObjects[i]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

		BrokenGlass *brokenGlass = dynamic_cast<BrokenGlass *>((Entity*) ctResult.collidedObjects[i]->getUserPointer());
		if (brokenGlass) {
			brokenGlass->CheckStructure();
			continue;
		}
		Glass *glass = dynamic_cast<Glass *>((Entity*) ctResult.collidedObjects[i]->getUserPointer());
		if (glass) {
			glass->CheckStructure();
			continue;
		}

		pair = attachedRBs.find(ctResult.collidedObjects[i]);
		if (ctResult.collidedObjects[i]->getUserPointer() != this || (pair != attachedRBs.end() && pair->second)) {
			attachedRBs[rb] = true;
			for (int j = 0; j < ctResult.collidedObjects.size(); ++j) {
				if (!ctResult.collidedObjects[j]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

				attachedRBs[ctResult.collidedObjects[j]] = true;
			}
			return true;
		}
	}

	for (int i = 0; i < ctResult.collidedObjects.size(); ++i) {
		if (!ctResult.collidedObjects[i]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

		BrokenGlass *brokenGlass = dynamic_cast<BrokenGlass *>((Entity*) ctResult.collidedObjects[i]->getUserPointer());
		if (brokenGlass) continue;
		Glass *glass = dynamic_cast<Glass *>((Entity*) ctResult.collidedObjects[i]->getUserPointer());
		if (glass) continue;

		if (IsAttached(ctResult.collidedObjects[i], rb)) {
			attachedRBs[rb] = true;
			for (int j = 0; j < ctResult.collidedObjects.size(); ++j) {
				if (!ctResult.collidedObjects[j]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

				attachedRBs[ctResult.collidedObjects[j]] = true;
			}
			return true;
		}

	}

	return false;
}