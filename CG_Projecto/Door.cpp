#include "stdafx.h"
#include "Door.h"

#include "PhysicsWorld.h"
#include "CharacterEntity.h"
#include "Trigger.h"

#define OPEN_Y_POS		1.9f
#define CLOSED_Y_POS	0.0f
#define SPEED			1.3f
#define SENSOR_RADIUS	3.5f

Door::Door(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, bool enabled, btTransform &transform, btVector3 &scale)
	:StaticEntity(world, name, transform, scale)
{

	btTransform trans;
	motionState->getWorldTransform(trans);
	trans.setOrigin(btVector3(trans.getOrigin()[0], 0.f, trans.getOrigin()[2]));
	doorSensor = std::make_unique<Trigger>(world, std::make_unique<btSphereShape>(SENSOR_RADIUS), trans);

	opening = false;
	this->enabled = enabled;
}

Door::~Door() {}

void Door::Update(float deltaTime) {
	if (!enabled) return;

	shared_ptr<CharacterEntity> p = dynamicsWorld->GetPlayer().lock();
	if (!p) return;

	if (doorSensor->TriggeredBy(p->GetRigidBody()))
		opening = true;
	else
		opening = false;

	if (opening) {
		btTransform trans;
		motionState->getWorldTransform(trans);

		if (trans.getOrigin()[1] < OPEN_Y_POS) {
			float newY = trans.getOrigin()[1] + SPEED * deltaTime;
			if (newY > OPEN_Y_POS) newY = OPEN_Y_POS;
			trans.setOrigin(btVector3(trans.getOrigin()[0], newY, trans.getOrigin()[2]));
		}
		motionState->setWorldTransform(trans);
		rigidBody->setCenterOfMassTransform(trans);
	}
	else{
		btTransform trans;
		motionState->getWorldTransform(trans);

		if (trans.getOrigin()[1] > CLOSED_Y_POS) {
			float newY = trans.getOrigin()[1] - SPEED * deltaTime;
			if (newY < CLOSED_Y_POS) newY = CLOSED_Y_POS;
			trans.setOrigin(btVector3(trans.getOrigin()[0], newY, trans.getOrigin()[2]));
		}
		motionState->setWorldTransform(trans);
		rigidBody->setCenterOfMassTransform(trans);
	}
}

void Door::SetActive(bool active) {
	enabled = active;
}