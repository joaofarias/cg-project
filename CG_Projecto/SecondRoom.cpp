#include "stdafx.h"
#include "SecondRoom.h"

#include "Game.h"
#include "PhysicsWorld.h"
#include "AssetsManager.h"
#include "StaticEntity.h"
#include "BreakableEntity.h"
#include "Dalek.h"
#include "FirstPersonCamera.h"
#include "Light.h"
#include "Trigger.h"
#include "DalekHUD.h"
#include "FirstRoom.h"
#include "Door.h"
#include "Wall.h"
#include "Glass.h"
#include "MirrorCube.h"
#include "Target.h"

SecondRoom::SecondRoom(std::shared_ptr<PhysicsWorld> world)
	: Level(world)
{
	currentCamera = 0;
	camAngle = 0.f;
}

SecondRoom::~SecondRoom() {}

void SecondRoom::Load() {
	LOG(TLogLevel::logERROR) << "This level needs a reference to the previous level";
	return;
}

void SecondRoom::Load(Level &previousLevel) {
	btTransform trans;
	trans.setIdentity();

	FirstRoom *previousRoom = dynamic_cast<FirstRoom*>(&previousLevel);
	if (!previousRoom) return;

	cameras.push_back(previousRoom->cameras[0]);
	currentCamera = 0;
	world->SetCamera(cameras[0]);
	cameras.push_back(std::make_shared<Camera>(90, (float) Game::GetScreenWidth() / Game::GetScreenHeight(), 0.001, 1000, btVector3(10, 5, -30), btVector3(0, 0, -30), btVector3(0, 1, 0)));
	cameras[1]->SetHUD(std::make_unique<HUD>(world->GetPlayer()));


	world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::SecondRoom));
	
	trans.setOrigin(btVector3(-1.f, 1.5f, -30.5f));
	trans.setRotation(btQuaternion(DEG2RAD(45), 0, AI_MATH_HALF_PI_F));
	world->AddEntity(std::make_shared<Wall>(world, AssetsManager::AssetName::SecondRoomWall, 200.f, trans));

	trans.setOrigin(btVector3(-2.77f, 0.5f, -28.73f));
	trans.setRotation(btQuaternion(DEG2RAD(45), 0, 0));
	world->AddEntity(std::make_shared<Glass>(world, AssetsManager::AssetName::Glass_4x2, trans), true);

	trans.setOrigin(btVector3(1.f, 1.5f, -30.5f));
	trans.setRotation(btQuaternion(DEG2RAD(-45), 0, -AI_MATH_HALF_PI_F));
	world->AddEntity(std::make_shared<Wall>(world, AssetsManager::AssetName::SecondRoomWall, 200.f, trans));

	trans.setOrigin(btVector3(2.77f, 0.5f, -28.73f));
	trans.setRotation(btQuaternion(DEG2RAD(-45), 0, 0));
	world->AddEntity(std::make_shared<Glass>(world, AssetsManager::AssetName::Glass_4x2, trans), true);

	trans.setOrigin(btVector3(0, 5.f, -30.f));
	trans.setRotation(btQuaternion(AI_MATH_PI_F, -AI_MATH_HALF_PI_F, 0));
	world->AddEntity(std::make_shared<MirrorCube>(world, AssetsManager::AssetName::Cube, 100.f, trans, btVector3(2, 2, 2)));

	for (int i = 0; i < 10; ++i) {
		trans.setOrigin(btVector3(rand() % 28 - 14, rand() % 8 + 1, -(rand() % 18 + 21)));
		trans.setRotation(btQuaternion(DEG2RAD(rand() % 360), DEG2RAD(rand() % 360), DEG2RAD(rand() % 360)));
		world->AddEntity(std::make_shared<Target>(world, AssetsManager::AssetName::Target, 5.f, trans));
	}

	/* Doors */

	trans.setIdentity();
	trans.setOrigin(btVector3(0, 0.f, -10.125f));
	world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::Door, trans));

	trans.setOrigin(btVector3(0, 0.f, -19.875f));
	world->AddEntity(std::make_shared<Door>(world, AssetsManager::AssetName::Door, true, trans));

	trans.setOrigin(btVector3(0, 0.f, -40.125f));
	world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::Door, trans));
}

void SecondRoom::Update(float deltaTime) {
	for each (sf::Event event in Game::events) {
		if (event.type == sf::Event::KeyReleased) {
			if (event.key.code == sf::Keyboard::C)
				world->SetCamera(cameras[++currentCamera % cameras.size()]);
		}
	}

	camAngle += 10 * deltaTime;
	if (camAngle >= 360.f) camAngle = 0.f;
	cameras[1]->SetPosition(btVector3(cosf(DEG2RAD(camAngle)) * 15.f, 9, sinf(DEG2RAD(camAngle)) * 10.f - 30.f));
}