#pragma once

#include "Entity.h"

class MultiEntity :
	public Entity
{
public:
	MultiEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~MultiEntity();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);

	virtual inline string GetDescription() { return "MultiEntity"; }
};

