#pragma once
#include "BreakableEntity.h"
#include "DynamicEntity.h"
class BrokenGlass :
	public BreakableEntity
{
public:
	BrokenGlass(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~BrokenGlass();

	virtual inline string GetDescription() { return "BrokenGlass"; }

protected:
	virtual void ToDynamic(btRigidBody *rb);

};

class GlassShard :
	public DynamicEntity {

	virtual inline string GetDescription() { return "GlassShard"; }

public:
	GlassShard(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds)
		: DynamicEntity(world, renderer, rendererMeshId, motionState, std::move(rigidBody), sounds) { }

};
