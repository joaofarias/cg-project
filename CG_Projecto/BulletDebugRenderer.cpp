#include "stdafx.h"
#include "BulletDebugRenderer.h"


BulletDebugRenderer::BulletDebugRenderer()
{
	m_debugMode = DebugDrawModes::DBG_DrawWireframe;
}


BulletDebugRenderer::~BulletDebugRenderer()
{
}

void BulletDebugRenderer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) {

	glEnable(GL_COLOR_MATERIAL);
	glColor4f(WHITE);
	glPushMatrix();
//	glScalef(10.f, 1.f, 10.f);
		glBegin(GL_LINES);
			glVertex3f(from.x(), from.y(), from.z());
			glVertex3f(to.x(), to.y(), to.z());
		glEnd();
	glPopMatrix();
}

int BulletDebugRenderer::getDebugMode() const {
	return m_debugMode;
}