#pragma once

class PhysicsWorld;

class Level
{
public:
	Level(std::shared_ptr<PhysicsWorld> world);
	~Level();

	virtual void Load() = 0;
	virtual void Load(Level &previousLevel) = 0;
	virtual void Update(float deltaTime) = 0;

protected:
	shared_ptr<PhysicsWorld> world;
};

