#include "stdafx.h"
#include "PhysicsWorld.h"
#include "BulletDebugRenderer.h"
#include "CharacterEntity.h"
#include "Camera.h"
#include "Game.h"
#include "Light.h"
#include "Level.h"
#include "Trigger.h"

static void tickCallback(btDynamicsWorld *world, btScalar timeStep);

PhysicsWorld::PhysicsWorld()
{
	collisionCfg = std::make_unique<btDefaultCollisionConfiguration>();
	collisionDispatcher = std::make_unique<btCollisionDispatcher>(collisionCfg.get());
	overlappingPairCache = std::make_unique<btDbvtBroadphase>();
	solver = std::make_unique<btSequentialImpulseConstraintSolver>();
	
	dynamicsWorld = std::make_unique<btDiscreteDynamicsWorld>(collisionDispatcher.get(), overlappingPairCache.get(), solver.get(), collisionCfg.get());

	dynamicsWorld->setGravity(btVector3(0, -9.80665f, 0));

	debugDrawer = std::make_unique<BulletDebugRenderer>();
	dynamicsWorld->setDebugDrawer(debugDrawer.get());
	dynamicsWorld->getSolverInfo().m_splitImpulse = true;
	dynamicsWorld->setInternalTickCallback(tickCallback, static_cast<void *>(this));

	loading = false;
	currentLevel = -1;
}

PhysicsWorld::~PhysicsWorld() {
	//cleanup in the reverse order of creation/initialization

	int i;
	//remove all constraints
	for (i = dynamicsWorld->getNumConstraints() - 1; i >= 0; i--)
	{
		btTypedConstraint* constraint = dynamicsWorld->getConstraint(i);
		dynamicsWorld->removeConstraint(constraint);
		delete constraint;
	}

	//remove the rigidbodies from the dynamics world and delete them

	for (i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		dynamicsWorld->removeCollisionObject(obj);
		delete obj;
	}
}

void PhysicsWorld::DrawWorld(float deltaTime) {
	camera->PrepareDrawing();

	// Update light positions
	for (int i = 0; i < lights.size(); ++i) {
		lights[i]->Update();
	}

	// Draw world
	player->Draw(deltaTime);
	for (auto it = entities.begin(); it != entities.end(); ++it) {
		it->get()->Draw(deltaTime);
	}

	currentStencilBit = 1;
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	for (auto it = transparentEntities.begin(); it != transparentEntities.end(); ++it) {
		if (it->get()->isReflective) {
			DrawReflection(it->get()->entity.get(), deltaTime, 0); 
		}
		else {
			glEnable(GL_BLEND);
			it->get()->entity->Draw(deltaTime);
			glDisable(GL_BLEND);
		}
	}

	// Debug
	//DebugDraw();

	// Draw HUD
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, Game::GetScreenWidth(), Game::GetScreenHeight(), 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	camera->DrawHUD(deltaTime);
}

void PhysicsWorld::DrawReflection(Entity *reflector, float deltaTime, int n) {
	if (n == 2) return;

	btVector3 tr = reflector->GetPosition() - camera->GetPosition();
	btVector3 fwd = reflector->GetForwardVector();
	if (tr.angle(fwd) >= AI_MATH_HALF_PI_F) return;
	
	// Draw reflective surface's mask on stencil buffer
	glEnable(GL_STENCIL_TEST);
	glColorMask(0, 0, 0, 0);
	glDepthMask(false);
	glStencilFunc(GL_ALWAYS, currentStencilBit, currentStencilBit);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	reflector->Draw(deltaTime);

	// Clear depth buffer inside reflective surface
	glDepthRange(1, 1);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(true);
	glStencilFunc(GL_EQUAL, currentStencilBit, currentStencilBit);
	currentStencilBit = currentStencilBit << 1;
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	reflector->Draw(deltaTime);

	// Draw reflected world
	glColorMask(1, 1, 1, 1);
	glDepthRange(0, 1);
	glDepthFunc(GL_LESS);
	glEnable(GL_CLIP_PLANE0 + n);

	tr = reflector->GetPosition();
	btQuaternion qr = reflector->GetRotation();
	btVector3 reflectionAxis = reflector->GetReflectionAxis();
	GLdouble clipPlane [] = { 0.f, 0.f, 1.f, 0.f };
	glPushMatrix();
		glTranslatef(tr[0], tr[1], tr[2]);
		glRotatef(RAD2DEG(qr.getAngle()), qr.getX(), qr.getY(), qr.getZ());
		
		glClipPlane(GL_CLIP_PLANE0 + n, clipPlane);

		//glScalef(1, 1, -1);
		glScalef(reflectionAxis[0], reflectionAxis[1], reflectionAxis[2]);
		glRotatef(RAD2DEG(-qr.getAngle()), qr.getX(), qr.getY(), qr.getZ());
		glTranslatef(-tr[0], -tr[1], -tr[2]);

		for (int i = 0; i < lights.size(); ++i) {
			lights[i]->Update();
		}

		DrawReflectedEntity(reflector, player.get(), deltaTime);

		for (auto itr = entities.begin(); itr != entities.end(); ++itr)
			DrawReflectedEntity(reflector, itr->get(), deltaTime);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		for (auto itr = transparentEntities.begin(); itr != transparentEntities.end(); ++itr) {
			if (itr->get()->isReflective && itr->get()->entity.get() != reflector && (n+1) < 2) {
				btVector3 tr = reflector->GetPosition() - itr->get()->entity.get()->GetPosition();
				btVector3 fwd = reflector->GetForwardVector();
				if (tr.angle(fwd) >= AI_MATH_HALF_PI_F) continue;
				glDisable(GL_BLEND);
				DrawReflection(itr->get()->entity.get(), deltaTime, n + 1);
				glEnable(GL_BLEND);
			}
			else {
				DrawReflectedEntity(reflector, itr->get()->entity.get(), deltaTime);
			}
		}

	glPopMatrix();

	glDisable(GL_CLIP_PLANE0 + n);

	glDisable(GL_STENCIL_TEST);
	glDisable(GL_BLEND);
}

void PhysicsWorld::DrawReflectedEntity(Entity *reflector, Entity *reflected, float deltaTime) {
	if (reflector == reflected) return;

	/*btVector3 tr = reflector->GetPosition() - reflected->GetPosition();
	btVector3 fwd = reflector->GetForwardVector(); 
	if (tr.angle(fwd) >= AI_MATH_HALF_PI_F) return;*/
	
	reflected->Draw(deltaTime, true, false);
}

void PhysicsWorld::Update(float deltaTime) {
	if (loading) {
		NextLevel();
		camera->OnLoadingEnd();
		loading = false;
		return;
	}
	else if (levelTrigger != nullptr && levelTrigger->TriggeredBy(player->GetRigidBody())) {
		camera->OnLoadingStart();
		loading = true;
		return;
	}

	dynamicsWorld->stepSimulation(deltaTime, 4);

	numActiveObjects = 0;
	btCollisionObjectArray objects = dynamicsWorld->getCollisionObjectArray();
	for (int i = 0, size = objects.size(); i < size; ++i) {
		if (objects[i]->isActive()) numActiveObjects++;
	}

	levels[currentLevel]->Update(deltaTime);

	player->Update(deltaTime);
	for (auto it = entities.begin(); it != entities.end(); ++it)
		it->get()->Update(deltaTime);
	for (auto it = transparentEntities.begin(); it != transparentEntities.end(); ++it) {
		it->get()->entity->Update(deltaTime);
	}

	RemoveEntities();

	for (auto it = transparentEntities.begin(); it != transparentEntities.end(); ++it)
		it->get()->distanceToCamera = camera->GetPosition().distance2(it->get()->entity->GetPosition());
	transparentEntities.sort(comp);

	camera->Update(deltaTime);
}

void PhysicsWorld::SetPlayer(shared_ptr<CharacterEntity> player) {
	this->player = player;
}

void PhysicsWorld::AddEntity(shared_ptr<Entity> entity, bool transparent, bool reflective) {
	if (transparent)
		transparentEntities.push_back(std::make_unique<TransparentEntityItem>(entity, reflective));
	else
		entities.push_back(entity);
}

void PhysicsWorld::AddLevel(shared_ptr<Level> level) {
	levels.push_back(level);
	if (levels.size() == 1)
		NextLevel();
}

void PhysicsWorld::RemoveEntities() {
	for (auto it = entities.begin(); it != entities.end();) {
		if (it->get()->IsReadyToDie()) {
			it->get()->RemoveFromWorld();
			entities.erase(it++);
		}
		++it;
	}

	for (auto it = transparentEntities.begin(); it != transparentEntities.end();) {
		if (it->get()->entity->IsReadyToDie()) {
			it->get()->entity->RemoveFromWorld();
			transparentEntities.erase(it++);
		}
		++it;
	}
}

int PhysicsWorld::GetNumActiveObjects() {
	return numActiveObjects;
}

void PhysicsWorld::SetCamera(shared_ptr<Camera> cam) {
	camera = cam;
}

void PhysicsWorld::AddLight(shared_ptr<Light> light) {
	lights.push_back(light);
}

void PhysicsWorld::SetLevelTrigger(unique_ptr<Trigger> trigger) {
	levelTrigger = std::move(trigger);
}

void PhysicsWorld::NextLevel() {
	if (currentLevel + 1 < levels.size()) {
		for (auto it = entities.begin(); it != entities.end(); ++it)
			it->get()->RemoveFromWorld();
		for (auto it = transparentEntities.begin(); it != transparentEntities.end(); ++it)
			it->get()->entity->RemoveFromWorld();
		for (auto it = lights.begin(); it != lights.end(); ++it) {
			it->get()->Disable();
		}
		entities.clear();
		transparentEntities.clear();
		lights.clear();
		levelTrigger = nullptr;

		Entity::CleanAssets();

		if (currentLevel + 1 == 0)
			levels[++currentLevel]->Load();
		else
			levels[++currentLevel]->Load(*levels[currentLevel-1]);
	}
}

btDiscreteDynamicsWorld *PhysicsWorld::GetWorld() {
	return dynamicsWorld.get();
}

weak_ptr<btMotionState> PhysicsWorld::GetPlayerMotionState() {
	return player->GetMotionState();
}

weak_ptr<CharacterEntity> PhysicsWorld::GetPlayer() {
	return weak_ptr<CharacterEntity>(player);
}

void PhysicsWorld::DebugDraw() {
	dynamicsWorld->debugDrawWorld();
}

void PhysicsWorld::OnSimulationTick(btScalar timeStep) {
	int numof_manifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i<numof_manifolds; ++i)
	{
		btPersistentManifold* contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);	
		if (contactManifold->getNumContacts() == 0)
			continue;
		if (dynamicsWorld->getCollisionObjectArray().findLinearSearch((btCollisionObject*) contactManifold->getBody0()) == dynamicsWorld->getCollisionObjectArray().size() ||
			dynamicsWorld->getCollisionObjectArray().findLinearSearch((btCollisionObject*) contactManifold->getBody1()) == dynamicsWorld->getCollisionObjectArray().size())
			continue;

		if (!contactManifold->getBody0() || !contactManifold->getBody1()) continue;
		btCollisionObject* obA = (btCollisionObject*) contactManifold->getBody0();
		btCollisionObject* obB = (btCollisionObject*) contactManifold->getBody1();
		if (!obA || !obB) continue;
		btRigidBody* bodyA = btRigidBody::upcast(obA);
		btRigidBody* bodyB = btRigidBody::upcast(obB);
		if (!bodyA || !bodyB) continue;
		Entity *entA = dynamic_cast<Entity *>((Entity *) obA->getUserPointer());
		Entity *entB = dynamic_cast<Entity *>((Entity *) obB->getUserPointer());
		if (!entA || !entB) continue;
		if (!entA->HasCollisionCallback() && !entB->HasCollisionCallback()) continue;

		int numof_contacts = contactManifold->getNumContacts();
		for (int j = 0; j<numof_contacts; ++j) {
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance() <= 0.001f) {
				
				if (entA->HasCollisionCallback())
					entA->OnCollision((btRigidBody *) obB, pt);
				
				if (entB->HasCollisionCallback())
					entB->OnCollision((btRigidBody *) obA, pt);
			}
		}
	}
	
}

PhysicsWorld::TransparentEntityItem::TransparentEntityItem(shared_ptr<Entity> ent, bool isReflective) {
	entity = ent;
	this->isReflective = isReflective;
}

static void tickCallback(btDynamicsWorld *world, btScalar timeStep) {
	PhysicsWorld *physEngine = static_cast<PhysicsWorld *>(world->getWorldUserInfo());
	physEngine->OnSimulationTick(timeStep);
}