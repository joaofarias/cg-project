#include "stdafx.h"
#include "Light.h"

bool Light::usedLights[8];

Light::Light(btVector4 &amb, btVector4 &diff, btVector4 &spec, btVector4 &pos, bool enabled)
{
	Vec4ToArray(pos, position);
	Vec4ToArray(amb, ambient);
	Vec4ToArray(diff, diffuse);
	Vec4ToArray(spec, specular);
	spotLight = false;
	numLight = -1;

	if (enabled)
		Enable();
}

Light::Light(btVector4 &amb, btVector4 &diff, btVector4 &spec, btVector4 &pos, btVector3 &dir, float intensity, float angle, bool enabled) {
	Vec4ToArray(amb, ambient);
	Vec4ToArray(diff, diffuse);
	Vec4ToArray(spec, specular);
	Vec4ToArray(pos, position);
	Vec3ToArray(dir, direction);
	spotLight = true;

	if (intensity < 0 || intensity > 128)
		this->intensity = 64;
	else
		this->intensity = intensity;
	
	if (angle < 0 || angle > 90)
		this->angle = 45;
	else
		this->angle = angle;
	numLight = -1;

	if (enabled)
		Enable();
}

Light::~Light() {
	Disable();
}

void Light::Update() {
	if (numLight == -1) return;

	glPushMatrix();
		glTranslatef(position[0], position[1], position[2]);
		GLfloat pos[] = { 0.0, 0.0, 0.0, position[3] };
		glLightfv(GL_LIGHT0 + numLight, GL_POSITION, pos);
	glPopMatrix();

	if (spotLight) {
		glLightfv(GL_LIGHT0 + numLight, GL_SPOT_DIRECTION, direction);		
	}
}

void Light::Enable() {
	if (numLight == -1) {
		numLight = GetAvailableLight();
		if (numLight == -1) {
			LOG(TLogLevel::logWARNING) << "No available lights to enable. Light ignored.";
			return;
		}
	}

	light = GL_LIGHT0 + numLight;
	glLightfv(light, GL_AMBIENT, ambient);
	glLightfv(light, GL_DIFFUSE, diffuse);
	glLightfv(light, GL_SPECULAR, specular);
	glLightfv(light, GL_POSITION, position);

	if (spotLight) {
		glLightfv(light, GL_SPOT_DIRECTION, direction);
		glLightf(light, GL_SPOT_EXPONENT, intensity);
		glLightf(light, GL_SPOT_CUTOFF, angle);
	}
	else
		glLightf(light, GL_SPOT_CUTOFF, 180);	// this should be enough to not be a spotlight anymore
												// check if the other parameter change anything

	glEnable(light);
	usedLights[numLight] = true;
}

void Light::Disable() {
	if (numLight == -1) return;
	glDisable(light);
	usedLights[numLight] = false;
	numLight = -1;
}

void Light::SetPosition(btVector4 &pos) {
	Vec4ToArray(pos, position);
}

void Light::SetDirection(btVector3 &dir) {
	Vec3ToArray(dir, direction);
}

void Light::SetIntensity(float intensity) {
	if (intensity < 0 || intensity > 128)
		this->intensity = 64;
	else
		this->intensity = intensity;
}

void Light::SetAngle(float angle) {
	if (angle < 0 || angle > 90)
		this->angle = 45;
	else
		this->angle = angle;
}