#pragma once
#include "BreakableEntity.h"
class Wall :
	public BreakableEntity
{
public:
	Wall(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Wall();

	virtual inline string GetDescription() { return "Wall"; }

private:
	virtual bool IsAttached(btRigidBody *rb, btRigidBody *from);
};

