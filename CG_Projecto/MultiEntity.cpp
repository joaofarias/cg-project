#include "stdafx.h"
#include "MultiEntity.h"

#include "PhysicsWorld.h"
#include "Renderer.h"
#include "StaticEntity.h"
#include "DynamicEntity.h"

MultiEntity::MultiEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
	: Entity(world, name, scale)
{
	shared_ptr<Renderer> renderer = std::make_shared<Renderer>(Entity::renderer->GetModelPath());

	btCompoundShape *compound = dynamic_cast<btCompoundShape*>(shape.get());
	if (!compound) return;

	btVector3 inertia(0, 0, 0);
	btTransform t;
	for (int i = 0; i < compound->getNumChildShapes(); ++i) {
		renderer->CenterOnOrigin(i, compound->getChildTransform(i));

		if (mass > 0)
			compound->getChildShape(i)->calculateLocalInertia(mass, inertia);
		t.setOrigin(compound->getChildTransform(i).getOrigin() + transform.getOrigin());
		t.setRotation(compound->getChildTransform(i).getRotation() + transform.getRotation());
		shared_ptr<btMotionState> ms = std::make_shared<btDefaultMotionState>(t);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, ms.get(), compound->getChildShape(i), inertia);
		unique_ptr<btRigidBody> rb = std::make_unique<btRigidBody>(rbInfo);

		if (mass == 0)
			dynamicsWorld->AddEntity(std::make_shared<StaticEntity>(dynamicsWorld, renderer, i, ms, std::move(rb), sounds));
		else
			dynamicsWorld->AddEntity(std::make_shared<DynamicEntity>(dynamicsWorld, renderer, i, ms, std::move(rb), sounds));
	}
	readyToDie = true;
}

MultiEntity::~MultiEntity() {}

void MultiEntity::Draw(float deltaTime, bool lighting, bool cullface) {}

void MultiEntity::Update(float deltaTime) {}