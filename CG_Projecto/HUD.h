#pragma once

#include "SFML\Graphics.hpp"

#define ASSETS_FILE_DIRECTORY	"..\\assets\\"
#define HUD_FILE_NAME			"hud.xml"
#define TEXTURE_TAG_NAME		"tex"

class Renderer;
class CharacterEntity;

class HUD
{
public:
	HUD(weak_ptr<CharacterEntity> player, string configFileName = HUD_FILE_NAME);
	~HUD();

	virtual void Draw(float deltaTime);
	virtual void Update(float deltaTime);
	virtual void OnLoadingStart();
	virtual void OnLoadingEnd();

protected:
	Poco::AutoPtr<Poco::XML::Document> hudConfig;
	std::vector<GLuint> textures;
	std::unordered_map<string, sf::Font> fonts;
	weak_ptr<CharacterEntity> player;

private:
	void LoadTextures();
	void LoadFonts();

};

