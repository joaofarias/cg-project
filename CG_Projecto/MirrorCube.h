#pragma once
#include "DynamicEntity.h"

class MirrorCube :
	public DynamicEntity
{
public:
	MirrorCube(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~MirrorCube();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true) {}
	virtual inline string GetDescription() { return "MirrorCube"; }

};

class MirrorCubeFace :
	public DynamicEntity {

	virtual inline string GetDescription() { return "MirrorCubeFace"; }
	virtual btVector3 GetPosition();
	btQuaternion GetRotation();
	virtual btVector3 GetForwardVector();
	virtual btVector3 GetReflectionAxis();


	btVector3 scale;
public:
	MirrorCubeFace(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds, btVector3 &scale);

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	void RemoveFromWorld();
};
