#pragma once

#include "Entity.h"

class StaticEntity :
	public Entity
{
public:
	StaticEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	StaticEntity(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds);
	~StaticEntity();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);

	virtual inline string GetDescription() { return "StaticEntity"; }
};

