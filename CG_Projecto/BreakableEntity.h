#pragma once
#include "Entity.h"
class BreakableEntity :
	public Entity
{
public:
	BreakableEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	BreakableEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, bool transparent, bool reflective = false, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~BreakableEntity();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);
	virtual void OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint);
	virtual void CheckStructure();
	virtual void RemoveFromWorld();

	virtual inline string GetDescription() { return "BreakableEntity"; }

protected:
	shared_ptr<Renderer> renderer;
	std::vector<shared_ptr<Entity>> entities;
	std::vector<shared_ptr<btMotionState>> motionStates;
	std::vector<unique_ptr<btRigidBody>> rigidBodies;
	btScalar mass;

	std::vector<float> timeInactive;
	float timeToDisappear;
	bool checkStructure;
	std::unordered_map<btRigidBody *, bool> attachedRBs;

	bool transparent, reflective;
	bool canPlaySound;

	virtual bool IsAttached(btRigidBody *rb, btRigidBody *from);
	virtual void ToDynamic(btRigidBody *rb);

	struct CollisionTestResult : btCollisionWorld::ContactResultCallback {
		btRigidBody *testingObject;
		btAlignedObjectArray<btRigidBody *> collidedObjects;
		virtual btScalar addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1);
	};

	typedef std::unordered_map<btRigidBody *, bool>::const_iterator HashPair;
};

