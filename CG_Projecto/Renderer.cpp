#include "stdafx.h"
#include "Renderer.h"
#include "IL\il.h"

Renderer::Renderer(string modelName)
{
	this->modelName = modelName;
	ImportModel();
}

Renderer::~Renderer() {}

void Renderer::Draw(unsigned int mesh, bool lighting, bool cullFace) {
	if (scene == nullptr)
		return;
	if (mesh >= 0 && mesh < scene->mRootNode->mNumChildren)
		Render(scene->mRootNode->mChildren[mesh], lighting, cullFace);
}

void Renderer::Draw(bool lighting, bool cullFace) {
	if (scene == nullptr)
		return;
	Render(scene->mRootNode, lighting, cullFace);
}
unsigned int Renderer::GetNumMeshes() {
	return scene->mNumMeshes;
}

void Renderer::GetVerticesFromMesh(unsigned int meshIndex, btAlignedObjectArray<btVector3> &vertices, btAlignedObjectArray<btVector3> &triangleIndices) {
	if (meshIndex >= scene->mNumMeshes || meshIndex < 0)
		return;
	
	vertices.clear();
	triangleIndices.clear();
	aiMesh *mesh = scene->mMeshes[meshIndex];
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		vertices.push_back(btVector3(btScalar(mesh->mVertices[i].x), btScalar(mesh->mVertices[i].y), btScalar(mesh->mVertices[i].z)));
	
	for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
		triangleIndices.push_back(btVector3(btScalar(mesh->mFaces[i].mIndices[0]), btScalar(mesh->mFaces[i].mIndices[1]), btScalar(mesh->mFaces[i].mIndices[2])));
	
}

void Renderer::GetVerticesFromMesh(unsigned int meshIndex, btAlignedObjectArray<btVector3> &vertices) {	
	btAlignedObjectArray<btVector3> ignore;
	GetVerticesFromMesh(meshIndex, vertices, ignore);
}

void Renderer::CenterOnOrigin(unsigned int meshIndex, btTransform &com) {
	if (meshIndex >= scene->mNumMeshes || meshIndex < 0)
		return;

	aiMesh *mesh = scene->mMeshes[meshIndex];
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i) {
		mesh->mVertices[i].x -= com.getOrigin().x();
		mesh->mVertices[i].y -= com.getOrigin().y();
		mesh->mVertices[i].z -= com.getOrigin().z();
	}
}

string Renderer::GetModelPath() {
	return modelName;
}

#include "assimp\Exporter.hpp"
void Renderer::ImportModel() {
	LOG(TLogLevel::logINFO) << "Importing model " << modelName;
	
	std::ifstream file(modelName);
	if (!file.fail())
		file.close();
	else
		return;

	scene = importer.ReadFile(modelName, aiProcessPreset_TargetRealtime_MaxQuality);
	if (!scene) {
		LOG(TLogLevel::logERROR) << "Couldn't load model from file: " << modelName << "\n\t\t" << importer.GetErrorString();
		return;
	}

	LoadGLTextures();

	LOG(TLogLevel::logINFO) << "Model " << modelName << " loaded successfully.";
}

void Renderer::ExportModel() {
	Assimp::Exporter exp = Assimp::Exporter();

	if (exp.Export(scene, "obj", modelName) != AI_SUCCESS) {
		LOG(TLogLevel::logERROR) << "Error exporting model asset: " << modelName;
	}
}

bool Renderer::LoadGLTextures()
{
	ILboolean success;

	/* Before calling ilInit() version should be checked. */
	if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION)
	{
		ILint test = ilGetInteger(IL_VERSION_NUM);
		/// wrong DevIL version ///
		LOG(TLogLevel::logERROR) << "Wrong DevIL version. Old devil.dll in system32/SysWow64?";
		return false;
	}

	ilInit(); /* Initialization of DevIL */

	if (scene->HasTextures()) LOG(TLogLevel::logWARNING) << "Support for meshes with embedded textures is not implemented";

	/* getTexture Filenames and Numb of Textures */
	for (unsigned int m = 0; m<scene->mNumMaterials; m++)
	{
		if (scene->mMaterials[m]->GetTextureCount(aiTextureType_DIFFUSE) == 0)
			continue;

		int texIndex = 0;
		aiReturn texFound = AI_SUCCESS;

		aiString path;	// filename

		while (texFound == AI_SUCCESS)
		{
			texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
			textureIdMap[path.data] = NULL; //fill map with textures, pointers still NULL yet
			texIndex++;
		}
	}

	int numTextures = textureIdMap.size();

	/* array with DevIL image IDs */
	ILuint* imageIds = NULL;
	imageIds = new ILuint[numTextures];

	/* generate DevIL Image IDs */
	ilGenImages(numTextures, imageIds); /* Generation of numTextures image names */

	/* create and fill array with GL texture ids */
	textureIds = new GLuint[numTextures];
	glGenTextures(numTextures, textureIds); /* Texture name generation */

	/* get iterator */
	std::map<std::string, GLuint*>::iterator itr = textureIdMap.begin();

	for (int i = 0; i<numTextures; i++)
	{

		//save IL image ID
		std::string filename = (*itr).first;  // get filename
		(*itr).second = &textureIds[i];	  // save texture id for filename in map
		itr++;								  // next texture


		ilBindImage(imageIds[i]); /* Binding of DevIL image name */
		std::string fileloc = filename;	/* Loading of image */
		success = ilLoadImage((wchar_t*) fileloc.c_str());

		if (success) /* If no error occured: */
		{
			success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); /* Convert every colour component into
																unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
			if (!success)
			{
				/* Error occured */
				LOG(TLogLevel::logERROR) << "Couldn't convert image";
				return false;
			}
			//glGenTextures(numTextures, &textureIds[i]); /* Texture name generation */
			glBindTexture(GL_TEXTURE_2D, textureIds[i]); /* Binding of texture name */
			//redefine standard texture values
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear
																			  interpolation for magnification filter */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear
																			  interpolation for minifying filter */
			glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH),
				ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
				ilGetData()); /* Texture specification */
		}
		else
		{
			/* Error occured */
			LOG(TLogLevel::logERROR) << "Couldn't load Image: " << fileloc;
		}


	}

	ilDeleteImages(numTextures, imageIds); /* Because we have already copied image data into texture data
										   we can release memory used by image. */

	//Cleanup
	delete[] imageIds;
	imageIds = NULL;

	//return success;
	return TRUE;
}

void Renderer::Render(const aiNode *node, bool lighting, bool cullFace) {
	const struct aiMesh *mesh;
	const struct aiFace *face;
	GLenum face_mode;
	int vertexIndex;

	glPushMatrix();

	// draw all meshes assigned to this node
	for (size_t i = 0; i < node->mNumMeshes; ++i) {
		mesh = scene->mMeshes[node->mMeshes[i]];

		// apply material
		ApplyMaterial(scene->mMaterials[mesh->mMaterialIndex], cullFace);

		if (mesh->mNormals != nullptr && lighting)
			glEnable(GL_LIGHTING);
		else
			glDisable(GL_LIGHTING);

		if (mesh->mColors[0] != nullptr)
			glEnable(GL_COLOR_MATERIAL);
		else
			glDisable(GL_COLOR_MATERIAL);

		if (scene->mMaterials[mesh->mMaterialIndex]->GetTextureCount(aiTextureType_DIFFUSE) != 0)
			glEnable(GL_TEXTURE_2D);
		else
			glDisable(GL_TEXTURE_2D);

		for (size_t j = 0; j < mesh->mNumFaces; ++j) {
			face = &mesh->mFaces[j];
			switch (face->mNumIndices) {
				case 1:		face_mode = GL_POINTS;		break;
				case 2:		face_mode = GL_LINES;		break;
				case 3:		face_mode = GL_TRIANGLES;	break;
				default:	face_mode = GL_POLYGON;		break;
			}

			glBegin(face_mode);

			for (size_t k = 0; k < face->mNumIndices; ++k) {
				vertexIndex = face->mIndices[k];
				if (mesh->mColors[0] != nullptr)
					Color4f(&mesh->mColors[0][vertexIndex]);
				if (mesh->mNormals != nullptr)
					glNormal3fv(&mesh->mNormals[vertexIndex].x);
				if (mesh->HasTextureCoords(0))
					glTexCoord2f(mesh->mTextureCoords[0][vertexIndex].x, 1 - mesh->mTextureCoords[0][vertexIndex].y);
				glVertex3fv(&mesh->mVertices[vertexIndex].x);
			}

			glEnd();
		}

	}

	// draw all children
	for (size_t i = 0; i < node->mNumChildren; ++i) {
		Render(node->mChildren[i], lighting, cullFace);
	}

	glPopMatrix();
}

void Renderer::ApplyMaterial(const aiMaterial *mtl, bool cullFace) {
	float color[4];
	GLenum fill_mode;
	int ret1, ret2;
	aiColor4D diffuse;
	aiColor4D specular;
	aiColor4D ambient;
	aiColor4D emission;
	float shininess, strength;
	int two_sided;
	int wireframe;	
	unsigned int max;

	int texIndex = 0;
	aiString texPath;
	if (mtl->GetTexture(aiTextureType_DIFFUSE, texIndex, &texPath) == aiReturn_SUCCESS) {
		// bind texture
		unsigned int texId = *textureIdMap[texPath.data];
		glBindTexture(GL_TEXTURE_2D, texId);
	}

	FillColorArray(color, 0.8f, 0.8f, 0.8f, 1.0f);
	if (aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse) == aiReturn_SUCCESS)
		Color4DToFloatArray(&diffuse, color);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);

	FillColorArray(color, 0.0f, 0.0f, 0.0f, 1.0f);
	if (aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular) == aiReturn_SUCCESS)
		Color4DToFloatArray(&specular, color);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);

	FillColorArray(color, 0.2f, 0.2f, 0.2f, 1.0f);
	if (aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient) == aiReturn_SUCCESS)
		Color4DToFloatArray(&ambient, color);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);

	FillColorArray(color, 0.0f, 0.0f, 0.0f, 1.0f);
	if (aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission) == aiReturn_SUCCESS)
		Color4DToFloatArray(&emission, color);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, color);

	max = 1;
	ret1 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
	if (ret1 == aiReturn_SUCCESS) {
		max = 1;
		ret2 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS_STRENGTH, &strength, &max);
		if (ret2 == aiReturn_SUCCESS)
			glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess * strength);
		else
			glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	} else {
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);
		FillColorArray(color, 0.0f, 0.0f, 0.0f, 0.0f);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);
	}

	max = 1;
	if (aiReturn_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_ENABLE_WIREFRAME, &wireframe, &max))
		fill_mode = wireframe ? GL_LINE : GL_FILL;
	else
		fill_mode = GL_FILL;
	glPolygonMode(GL_FRONT_AND_BACK, fill_mode);

	max = 1;
	if ((aiReturn_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_TWOSIDED, &two_sided, &max)) && two_sided || !cullFace)
		glDisable(GL_CULL_FACE);
	else
		glEnable(GL_CULL_FACE);
}