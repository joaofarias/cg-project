#include "stdafx.h"
#include "DalekHUD.h"

#include "Game.h"
#include "Dalek.h"

using namespace Poco::XML;

DalekHUD::DalekHUD(weak_ptr<CharacterEntity> player)
	: HUD(player, "dalekHUD.xml")
{
	if (!LoadNextMission()) {
		textEnabled = false;
		LOG(TLogLevel::logERROR) << "Unable to load missions from hud file.";
	}
	else 
		textEnabled = true;

	missionText.setFont(fonts["consola"]);
	missionText.setCharacterSize(25);
	missionText.setColor(sf::Color::White);
	missionText.setPosition(50.f, Game::GetScreenHeight() - (Game::GetScreenHeight() * 0.12f));

	objectiveText.setFont(fonts["consola"]);
	objectiveText.setCharacterSize(25);
	objectiveText.setColor(sf::Color::White);
	objectiveText.setPosition(50.f, Game::GetScreenHeight() - (Game::GetScreenHeight() * 0.074f));

	activatingView = viewActivated = false;
	actions["enableView"] = std::bind(&DalekHUD::EnableView, this);

	activatingCrosshair = crosshairActivated = false;
	actions["enableHUD"] = std::bind(&DalekHUD::EnableHUD, this);

	actions["enableLookAndGun"] = std::bind(&DalekHUD::EnableLookAndGun, this);

	if (currentObjective.action != "" && currentObjective.actionOnStart) actions[currentObjective.action]();

	loading = false;
}

DalekHUD::~DalekHUD() {}

void DalekHUD::Draw(float deltaTime) {
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	
	if (loading) {
		for (int i = 0; i < 2; ++i) {
			glBindTexture(GL_TEXTURE_2D, textures[i]);

			glBegin(GL_POLYGON);
				glTexCoord2f(0.f, 0.f);	glVertex2f(0.f, 0.f);
				glTexCoord2f(0.f, 1.f);	glVertex2f(0.f, (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 1.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 0.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), 0.f);
			glEnd();
		}
	}
	else if (viewActivated && crosshairActivated || !textEnabled) {
		for (int i = 3; i < textures.size(); i++) {
			glBindTexture(GL_TEXTURE_2D, textures[i]);

			glBegin(GL_POLYGON);
				glTexCoord2f(0.f, 0.f);	glVertex2f(0.f, 0.f);
				glTexCoord2f(0.f, 1.f);	glVertex2f(0.f, (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 1.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 0.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), 0.f);
			glEnd();
		}
	}
	else if (activatingView || activatingCrosshair){
		FadingIn(deltaTime);
	}
	else {
		glDisable(GL_TEXTURE_2D);

		glColor4f(0.f, 0.f, 0.f, 1.f);
		glBegin(GL_POLYGON);
			glVertex2f(0.f, 0.f);
			glVertex2f(0.f, (GLfloat) Game::GetScreenHeight());
			glVertex2f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight());
			glVertex2f((GLfloat) Game::GetScreenWidth(), 0.f);
		glEnd();

		glEnable(GL_TEXTURE_2D);
	}

	if (textEnabled) {
		Game::DrawString(missionText);
		Game::DrawString(objectiveText);
	}

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glEnable(GL_LIGHTING);
	glDisable(GL_BLEND);
}

void DalekHUD::FadingIn(float deltaTime) {
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	if (activatingView) {	
		if (currentObjective.progress < 50.f) {
			float op = currentObjective.progress / 100.f * 2.f;
			glColor4f(op, op, op, 1.f);
		}
		else {
			float op = 1 - ((currentObjective.progress - 50.f) * (0.85f/50));
			if (op <= 0.15f) op = 0.15f;
			glColor4f(1.f, 1.f, 1.f, op);
		}
		for (int i = 0; i < 2; ++i) {
			glBindTexture(GL_TEXTURE_2D, textures[i]);

			glBegin(GL_POLYGON);
				glTexCoord2f(0.f, 0.f);	glVertex2f(0.f, 0.f);
				glTexCoord2f(0.f, 1.f);	glVertex2f(0.f, (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 1.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 0.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), 0.f);
			glEnd();
		}
	}
	else if (activatingCrosshair) {
		for (int i = 0; i < 3; i++) {
			glBindTexture(GL_TEXTURE_2D, textures[i]);
			
			float op;
			if (i == 0) {
				if (currentObjective.progress > 75.f)
					op = 0.15f - (0.15f / 25.f) * (currentObjective.progress - 75);
				else
					op = 0.15f;
			}
			else if (i == 2) {
				op = 5.f - (currentObjective.progress / 100.f) * 5.f;
				if (op < 1.f) op = 1.0f;
				glPushMatrix();
					glTranslatef(Game::GetScreenWidth() / 2.f, Game::GetScreenHeight() / 2.f, 0.f);
					glScalef(op, op, 1.f);
					glRotatef(currentObjective.progress*180.f / 100.f, 0.f, 0.f, 1.f);
					glTranslatef(-Game::GetScreenWidth() / 2.f, -Game::GetScreenHeight() / 2.f, 0.f);

				op = 1.0f;
			}
			else op = 0.15f;
			
			glColor4f(1.f, 1.f, 1.f, op);
			
			glBegin(GL_POLYGON);
				glTexCoord2f(0.f, 0.f);	glVertex2f(0.f, 0.f);
				glTexCoord2f(0.f, 1.f);	glVertex2f(0.f, (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 1.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), (GLfloat) Game::GetScreenHeight());
				glTexCoord2f(1.f, 0.f);	glVertex2f((GLfloat) Game::GetScreenWidth(), 0.f);
			glEnd();

			if (i == 2) glPopMatrix();
		}

		if (currentObjective.currentTime + deltaTime > currentObjective.time) {
			activatingCrosshair = false;
			crosshairActivated = true;
		}
	}

}

void DalekHUD::Update(float deltaTime) {
	if (textEnabled == false) return;
	
	float oldProgress = currentObjective.progress;
	currentObjective.progress = currentObjective.currentTime * 100 / currentObjective.time;
	if (currentObjective.progress > 100.f) currentObjective.progress = 100.f;

	if (currentObjective.progress != oldProgress)
		currentMission.progress += (currentObjective.progress - oldProgress) * currentObjective.missionProgress / 100;
	if (currentMission.progress > 100.f) currentMission.progress = 100.f;
	

	missionText.setString(currentMission.str + std::to_string((int) floor(currentMission.progress)) + "%");
	objectiveText.setString(currentObjective.str + std::to_string((int) floor(currentObjective.progress)) + "%");

	if (currentMission.progress >= 100.f) {
		if (currentObjective.currentTime < currentObjective.time + 1.f) {	// 1sec pause
			currentObjective.currentTime += deltaTime;
			return;
		}
		
		if (currentObjective.action != "" && !currentObjective.actionOnStart) actions[currentObjective.action]();
		if (!LoadNextMission())
			textEnabled = false;
		if (currentObjective.action != "" && currentObjective.actionOnStart) actions[currentObjective.action]();
		currentObjective.currentTime -= deltaTime;
	} else if (currentObjective.progress >= 100.f) {
		if (currentObjective.currentTime < currentObjective.time + 0.5f) {	// 0.5sec pause
			currentObjective.currentTime += deltaTime;
			return;
		}
		
		if (currentObjective.action != "" && !currentObjective.actionOnStart) actions[currentObjective.action]();
		if (!LoadNextObjective())
			if (!LoadNextMission())
				textEnabled = false;
		if (currentObjective.action != "" && currentObjective.actionOnStart) actions[currentObjective.action]();
		currentObjective.currentTime -= deltaTime;
	}

	currentObjective.currentTime += deltaTime;
}

bool DalekHUD::LoadNextObjective() {
	Element *el = hudConfig->getElementById(std::to_string(currentMission.id) + "_" + std::to_string(currentMission.currentObjective+1), "objectiveId");
	if (el) {
		string str = el->getAttribute("string");
		int missionPercentage = 0;
		float time = 0.f;
		string action = "";
		bool actionOnStart = true;
		Element *child = el->getChildElement("missionPercentage");
		if (child) missionPercentage = std::stoi(child->getAttribute("value"));

		child = el->getChildElement("time");
		if (child) time = std::stof(child->getAttribute("value"));

		child = el->getChildElement("action");
		if (child) {
			action = child->getAttribute("method");
			actionOnStart = child->getAttribute("onStart") == "true" ? true : false;
		}

		currentObjective = Objective(1, str, missionPercentage, time, action, actionOnStart);
		currentMission.currentObjective++;
		return true;
	}
		
	return false;
}

bool DalekHUD::LoadNextMission() {
	Element *el = hudConfig->getElementById(std::to_string(currentMission.id + 1), "missionId");
	if (el) {
		currentMission = Mission(currentMission.id + 1, el->getAttribute("string"));
		if (!LoadNextObjective())
			return false;
		return true;
	}
	return false;
}

void DalekHUD::OnLoadingStart() {
	sf::Text loadingText = sf::Text("Loading...", fonts["consola"], 30);
	loadingText.setColor(sf::Color::White);
	loadingText.setOrigin(loadingText.getLocalBounds().width/2.f, loadingText.getLocalBounds().height/2.f);
	loadingText.setPosition(Game::GetScreenWidth() / 2.f, Game::GetScreenHeight() / 2.f);
	Game::DrawString(loadingText);

	loading = true;
}

void DalekHUD::OnLoadingEnd() {
	loading = false;
}

void DalekHUD::EnableView() {
	activatingView = true;
}

void DalekHUD::EnableHUD() {
	activatingView = false;
	viewActivated = true;
	activatingCrosshair = true;
}

void DalekHUD::EnableMovement() {
	shared_ptr<CharacterEntity> p = player.lock();
	if (!p) return;

	Dalek *dalek = dynamic_cast<Dalek*>(p.get());
	if (dalek) {
		dalek->ToggleLook(true);
		dalek->ToggleMovement(true);
	}
}

void DalekHUD::EnableLookAndGun() {
	shared_ptr<CharacterEntity> p = player.lock();
	if (!p) return;

	Dalek *dalek = dynamic_cast<Dalek*>(p.get());
	if (dalek) {
		dalek->ToggleLook(true);
		dalek->ToggleFiring(true);
	}
}