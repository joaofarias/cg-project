#include "stdafx.h"
#include "ShapeImporter.h"

#include "btBulletFile.h"

ShapeImporter::ShapeImporter()
	: btWorldImporter(0)
{}


ShapeImporter::~ShapeImporter() {}

btCollisionShape *ShapeImporter::GetShapeFromFile(const string &fileName) {
	bParse::btBulletFile *file = new bParse::btBulletFile(fileName.data());
	if (file->getFlags() & bParse::FD_OK) {
		file->parse(0);

		btCollisionShapeData* shapeData = (btCollisionShapeData*) file->m_collisionShapes[0];
		return convertCollisionShape(shapeData);
	}
	return nullptr;
}

btCollisionShape *ShapeImporter::GetShapeFromMemory(char *data, int size) {
	bParse::btBulletFile *file = new bParse::btBulletFile(data, size);
	if (file->getFlags() & bParse::FD_OK) {
		file->parse(0);

		btCollisionShapeData* shapeData = (btCollisionShapeData*) file->m_collisionShapes[0];
		return convertCollisionShape(shapeData);
	}
	return nullptr;
}