#pragma once

#include "AssetsManager.h"

class Renderer;
class PhysicsWorld;

class Entity
{
public:
	Entity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btVector3 &scale = btVector3(1, 1, 1));
	Entity(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds);
	~Entity();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true) = 0;
	virtual void Update(float deltaTime) = 0;
	virtual void OnCollision(btRigidBody *other, btManifoldPoint &contactPoint){}
	virtual void OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint){}
	virtual void RemoveFromWorld();

	weak_ptr<btMotionState> GetMotionState();
	virtual btVector3 GetPosition();
	virtual btQuaternion GetRotation();
	virtual btVector3 GetForwardVector();
	virtual inline btVector3 GetReflectionAxis() { return btVector3(1, 1, -1); }
	btRigidBody *GetRigidBody();
	bool HasCollisionCallback();
	bool IsReadyToDie();
	virtual inline string GetDescription() { return "Entity"; }


	static void CleanAssets();

protected:
	shared_ptr<PhysicsWorld> dynamicsWorld;
	shared_ptr<Renderer> renderer;
	shared_ptr<btCollisionShape> shape;
	shared_ptr<btMotionState> motionState;
	unique_ptr<btRigidBody> rigidBody;
	std::vector<sf::Sound> sounds;

	bool collisionCallback;
	bool readyToDie;
	int rendererMeshID;

	virtual void GenerateRigidBody(btTransform &transform, btScalar mass = 0);
	static AssetsManager assetsManager;
};

