#include "stdafx.h"
#include "DynamicEntity.h"

#include "Renderer.h"
#include "PhysicsWorld.h"

DynamicEntity::DynamicEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
	:Entity(world, name, scale)
{
	this->mass = mass;
	GenerateRigidBody(transform, mass);
}

DynamicEntity::DynamicEntity(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds)
	:Entity(world, renderer, rendererMeshId, motionState, std::move(rigidBody), sounds)
{}

DynamicEntity::~DynamicEntity() {}

void DynamicEntity::Draw(float deltaTime, bool lighting, bool cullface) {
	if (rigidBody == nullptr) return;

	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	const btVector3 scale = rigidBody->getCollisionShape()->getLocalScaling();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glPushMatrix();
		glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
		glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
		glScalef(scale.x(), scale.y(), scale.z());

		if (rendererMeshID < 0)	renderer->Draw(lighting, cullface);
		else					renderer->Draw(rendererMeshID, lighting, cullface);
	glPopMatrix();
	glDisable(GL_NORMALIZE);
}

void DynamicEntity::Update(float deltaTime) {}

void DynamicEntity::ApplyCentralImpulse(btVector3 &impulse) {
	rigidBody->applyCentralImpulse(impulse);
}