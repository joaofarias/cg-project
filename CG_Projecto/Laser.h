#pragma once
#include "DynamicEntity.h"

class Laser :
	public DynamicEntity
{
public:
	Laser(shared_ptr<PhysicsWorld> world, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Laser();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);
	virtual void OnCollision(btRigidBody *other, btManifoldPoint &contactPoint);

	virtual inline string GetDescription() { return "Laser"; }

private:
	btScalar velocity;
	btScalar explosionRadius;
	bool soundPlayed, exploded;

	struct ExplosionContactCallback : btCollisionWorld::ContactResultCallback {
		bool glass = false;
		bool target = false;
		virtual btScalar addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1);
	};
};

