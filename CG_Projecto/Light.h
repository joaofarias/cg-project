#pragma once

class Light
{
public:
	Light(btVector4 &amb, btVector4 &diff, btVector4 &spec, btVector4 &pos, bool enabled = true);
	Light(btVector4 &amb, btVector4 &diff, btVector4 &spec, btVector4 &pos, btVector3 &dir, float intensity, float angle, bool enabled = true);
	~Light();

	void Update();
	void Enable();
	void Disable();

	// TODO: create methods to change light's properties in real-time
	void SetPosition(btVector4 &pos);
	void SetDirection(btVector3 &dir);
	void SetIntensity(float intensity);
	void SetAngle(float angle);


private:

	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
	GLfloat direction[3];
	GLfloat intensity;
	GLfloat angle;
	bool spotLight;

	short numLight;
	GLenum light;

	static bool usedLights[8];
	inline short GetAvailableLight() {
		for (short i = 0; i < 8; i++)
			if (!usedLights[i])	return i;
		return -1;
	}

	inline void Vec4ToArray(btVector4 &from, GLfloat *to) {
		to[0] = from[0];
		to[1] = from[1];
		to[2] = from[2];
		to[3] = from[3];
	}

	inline void Vec3ToArray(btVector3 &from, GLfloat *to) {
		to[0] = from[0];
		to[1] = from[1];
		to[2] = from[2];
	}
};

