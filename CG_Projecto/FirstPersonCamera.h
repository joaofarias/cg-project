#pragma once

#include "Camera.h"
class CharacterEntity;

class FirstPersonCamera :
	public Camera
{
public:
	FirstPersonCamera();
	FirstPersonCamera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar);
	~FirstPersonCamera();

	virtual void Update(float deltaTime);
	
	void SetCharacterEntity(weak_ptr<CharacterEntity> charEntity);

private:
	weak_ptr<CharacterEntity> charEntity;
};

