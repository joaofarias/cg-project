#pragma once
#include "LinearMath\btIDebugDraw.h"

class BulletDebugRenderer : public btIDebugDraw
{
public:
	
	BulletDebugRenderer();
	~BulletDebugRenderer();

	virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color);
	virtual int  getDebugMode() const;

	virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {}
	virtual void reportErrorWarning(const char* warningString) {}
	virtual void draw3dText(const btVector3& location, const char* textString) {}
	virtual void setDebugMode(int debugMode) {}

private:
	int m_debugMode;
};

