#pragma once
#include "HUD.h"

#include "SFML\Graphics.hpp"

class DalekHUD :
	public HUD
{
public:
	DalekHUD(weak_ptr<CharacterEntity> player);
	~DalekHUD();

	virtual void Draw(float deltaTime);
	virtual void Update(float deltaTime);
	virtual void OnLoadingStart();
	virtual void OnLoadingEnd();

private:
	std::map<string, std::function<void(void)>> actions;
	bool activatingView, viewActivated;
	bool activatingCrosshair, crosshairActivated;
	bool textEnabled;
	bool loading;

	sf::Font textFont;
	sf::Text missionText;
	sf::Text objectiveText;

	void FadingIn(float deltaTime);
	bool LoadNextObjective();
	bool LoadNextMission();
	void EnableView();
	void EnableHUD();
	void EnableMovement();
	void EnableLookAndGun();

	struct Mission {
		int id;
		string str;
		float progress;
		int currentObjective;

		Mission() : id(0) {}
		Mission(int id, string str) : id(id), str(str), progress(0.f), currentObjective(0){}
	};

	struct Objective {
		int id;
		string str;
		int missionProgress;
		float progress;
		float currentTime;
		float time;
		string action;
		bool actionOnStart;

		Objective(){}
		Objective(int id, string str, int missionPercentage, float time, string action, bool actionOnStart)
			: id(id),
			str(str),
			missionProgress(missionPercentage),
			progress(0.f),
			currentTime(0.f),
			time(time),
			action(action),
			actionOnStart(actionOnStart) {}
	};
	
	Mission currentMission;
	Objective currentObjective;
};

