#pragma once

#include "Entity.h"

class PhysicsWorld;

class DynamicEntity :
	public Entity
{
public:
	DynamicEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	DynamicEntity(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds);
	~DynamicEntity();
	
	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);
	
	virtual inline string GetDescription() { return "DynamicEntity"; }

	void ApplyCentralImpulse(btVector3 &impulse);
protected:
	btScalar mass;

};

