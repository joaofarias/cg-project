#include "stdafx.h"
#include "Laser.h"

#include "Renderer.h"
#include "PhysicsWorld.h"
#include "Glass.h"
#include "BrokenGlass.h"
#include "Target.h"

Laser::Laser(shared_ptr<PhysicsWorld> world, btTransform &transform, btVector3 &scale)
	: DynamicEntity(world, AssetsManager::BlueLaser, 0.1f, transform, scale)
{
	velocity = 20.f;
	explosionRadius = 1.f;

	rigidBody->setGravity(btVector3(0, 0, 0));
	rigidBody->applyGravity();
	rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

	collisionCallback = true;
	
	soundPlayed = false;
}

Laser::~Laser() {}

void Laser::Draw(float deltaTime, bool lighting, bool cullface) {
	if (rigidBody == nullptr || exploded) return;
	
	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	const btVector3 scale = rigidBody->getCollisionShape()->getLocalScaling();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glPushMatrix();
		glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
		glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
		glScalef(scale.x(), scale.y(), scale.z());
		
		renderer->Draw(false, false);
		
	glPopMatrix();
	glDisable(GL_NORMALIZE);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void Laser::Update(float deltaTime) {
	if (!soundPlayed && sounds.size() >= 1) {
		sounds[0].play();
		soundPlayed = true;
	}
	if (soundPlayed && sounds[0].getStatus() == sf::Sound::Stopped && exploded) {
		readyToDie = true;
		return;
	}

	if (motionState == nullptr || rigidBody == nullptr) return;

	btTransform transform;
	motionState->getWorldTransform(transform);
	rigidBody->setLinearVelocity(transform.getBasis().getColumn(2) * velocity);
}

void Laser::OnCollision(btRigidBody *other, btManifoldPoint &contactPoint) {
	Glass *glass = dynamic_cast<Glass *>((Entity *) other->getUserPointer());
	if (glass) {
		glass->OnExplosion(rigidBody.get(), other, contactPoint);
		return;
	}

	dynamicsWorld->GetWorld()->removeRigidBody(rigidBody.get());

	btSphereShape *explosionShape = new btSphereShape(explosionRadius);
	btCollisionObject *explosionObject = new btCollisionObject();
	explosionObject->setCollisionFlags(explosionObject->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	explosionObject->setCollisionShape(explosionShape);
	explosionObject->setUserPointer(other->getUserPointer());
	btTransform transform;
	motionState->getWorldTransform(transform);
	explosionObject->setWorldTransform(transform);

	BrokenGlass *brokenGlass = dynamic_cast<BrokenGlass *>((Entity *) other->getUserPointer());
	GlassShard *glassShard = dynamic_cast<GlassShard *>((Entity *) other->getUserPointer());
	Target *target = dynamic_cast<Target *>((Entity *) other->getUserPointer());
	
	ExplosionContactCallback resultCallback;

	if (brokenGlass || glassShard)
		resultCallback.glass = true;
	else if (target)
		resultCallback.target = true;

	dynamicsWorld->GetWorld()->contactTest(explosionObject, resultCallback);

	delete explosionObject;
	delete explosionShape;

	if (brokenGlass || glassShard) {
		dynamicsWorld->GetWorld()->addRigidBody(rigidBody.get());
		return;
	}
	
	exploded = true;
}

btScalar Laser::ExplosionContactCallback::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1) {
	if (cp.getDistance() > 0.001f) return 0;

	btRigidBody *other = nullptr;
	btRigidBody *laser = nullptr;
	btVector3 contactPosition;

	if (!(colObj0Wrap->getCollisionObject()->getCollisionFlags() & btCollisionObject::CF_NO_CONTACT_RESPONSE)) {
		other = (btRigidBody *) colObj0Wrap->getCollisionObject();
		laser = (btRigidBody *) colObj1Wrap->getCollisionObject();
		contactPosition = cp.getPositionWorldOnA();
	}
	else if (!(colObj1Wrap->getCollisionObject()->getCollisionFlags() & btCollisionObject::CF_NO_CONTACT_RESPONSE)) {
		other = (btRigidBody *) colObj1Wrap->getCollisionObject();
		laser = (btRigidBody *) colObj0Wrap->getCollisionObject();
		contactPosition = cp.getPositionWorldOnB();
	}

	if (!other || !laser) return 0;

	BrokenGlass *brokenGlass = dynamic_cast<BrokenGlass *>((Entity *) other->getUserPointer());
	if (glass && !brokenGlass) return 0;

	Target *t = dynamic_cast<Target *>((Entity *) other->getUserPointer());
	if (target && !t) return 0;

	Entity *ent = dynamic_cast<Entity *>((Entity *) other->getUserPointer());
	if (ent && ent->HasCollisionCallback())
		ent->OnExplosion(laser, other, cp);
		
	other->activate(true);
	btVector3 imp = (laser->getWorldTransform().getOrigin() - other->getWorldTransform().getOrigin()).normalized() * 5.f / cp.getDistance();
	other->applyImpulse(imp, contactPosition);	

	return cp.getDistance();
}