#include "stdafx.h"
#include "FirstRoom.h"

#include "Game.h"
#include "PhysicsWorld.h"
#include "AssetsManager.h"
#include "StaticEntity.h"
#include "BreakableEntity.h"
#include "Dalek.h"
#include "FirstPersonCamera.h"
#include "Light.h"
#include "Trigger.h"
#include "DalekHUD.h"
#include "Door.h"
#include "Glass.h"
#include "Target.h"
#include "Wall.h"

#define FONT_PATH	"..\\assets\\fonts\\consola.ttf"

FirstRoom::FirstRoom(std::shared_ptr<PhysicsWorld> world)
	: Level(world)
{
	currentCamera = 0;
	leftTargetHit = middleTargetHit = rightTargetHit = false;
	levelDone = false;

	camAngle = 0.f;
}

FirstRoom::~FirstRoom() {}

void FirstRoom::Load() {
	GLfloat ambLight[] = { 0.f, 0.f, 0.f, 1.f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambLight);

	btTransform trans;
	trans.setIdentity();

	// Player
	trans.setOrigin(btVector3(0, 0.f, 5.f));
	trans.setRotation(btQuaternion(AI_MATH_PI_F, 0, 0));
	world->SetPlayer(std::make_shared<Dalek>(world, 400.f, trans, btVector3(0.8f, 0.8f, 0.8f)));

	cameras.push_back(std::make_shared<FirstPersonCamera>(90, (float) Game::GetScreenWidth() / Game::GetScreenHeight(), 0.001, 1000));
	world->SetCamera(cameras.back());
	cameras.push_back(std::make_shared<Camera>(90, (float) Game::GetScreenWidth() / Game::GetScreenHeight(), 0.001, 1000, btVector3(10, 5, 0), btVector3(0, 0, 0), btVector3(0, 1, 0)));
	((FirstPersonCamera *) cameras[0].get())->SetCharacterEntity(world->GetPlayer());
	cameras[0]->SetHUD(std::make_unique<DalekHUD>(world->GetPlayer()));
	cameras[1]->SetHUD(std::make_unique<HUD>(world->GetPlayer()));

	lights.push_back(std::make_shared<Light>(btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(0, 5.9f, 5, 1), btVector3(0, -6, 0), 20.f, 45.f));
	world->AddLight(lights.back());
	lights.push_back(std::make_shared<Light>(btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(0, 0, 0, 1), btVector3(0, 3, -5), 40.f, 30.f));
	world->AddLight(lights.back());
	
	lights.push_back(std::make_shared<Light>(btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(5, 5.5f, 0, 1), btVector3(0, -3, -5), 10.f, 90.f, false));
	world->AddLight(lights.back());
	lights.push_back(std::make_shared<Light>(btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(-5, 5.5f, 0, 1), btVector3(0, -3, -5), 10.f, 90.f, false));
	world->AddLight(lights.back());
	lights.push_back(std::make_shared<Light>(btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(1, 1, 1, 1), btVector4(0, 3.9f, -15, 1), btVector3(0, -4, 0), 10.f, 60.f, false));
	world->AddLight(lights.back());
	

	world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::FirstRoom));

	trans.setIdentity();
	trans.setOrigin(btVector3(0, 0.f, -10.125f));
	door = std::make_shared<Door>(world, AssetsManager::AssetName::Door, false, trans);
	world->AddEntity(door);

	trans.setOrigin(btVector3(0, 0.f, -19.875f));
	world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::Door, trans));

	trans.setOrigin(btVector3(0.f, 2.f, -5.f));
	middleTarget = std::make_shared<Target>(world, AssetsManager::AssetName::Target, 5.f, trans);
	world->AddEntity(middleTarget);

	trans.setOrigin(btVector3(-5.f, 0.f, -5.f));
	world->AddEntity(std::make_shared<Wall>(world, AssetsManager::AssetName::FirstRoomWall, 200.f, trans));

	trans.setOrigin(btVector3(-5.f, 3.f, -5.f));
	world->AddEntity(std::make_shared<Glass>(world, AssetsManager::AssetName::Glass_2x2, trans), true);

	trans.setOrigin(btVector3(-5.f, 4.f, -6.f));
	leftTarget = std::make_shared<Target>(world, AssetsManager::AssetName::Target, 5.f, trans);
	world->AddEntity(leftTarget);

	trans.setOrigin(btVector3(5.f, 0.f, -5.f));
	world->AddEntity(std::make_shared<Wall>(world, AssetsManager::AssetName::FirstRoomWall, 200.f, trans));

	trans.setOrigin(btVector3(5.f, 3.f, -5.f));
	world->AddEntity(std::make_shared<Glass>(world, AssetsManager::AssetName::Glass_2x2, trans), true);

	trans.setOrigin(btVector3(5.f, 4.f, -6.f));
	rightTarget = std::make_shared<Target>(world, AssetsManager::AssetName::Target, 5.f, trans);
	world->AddEntity(rightTarget);

	//trans.setOrigin(btVector3(-9.9f, 0.f, 0.f));
	//trans.setRotation(btQuaternion(-AI_MATH_HALF_PI_F, 0, 0));
	//world->AddEntity(std::make_shared<StaticEntity>(world, AssetsManager::AssetName::Mirror, trans, btVector3(20, 6, 1)), true, true);

	trans.setIdentity();
	trans.setOrigin(btVector3(0.f, 2.f, -18.f));
	world->SetLevelTrigger(std::make_unique<Trigger>(world, std::make_unique<btBoxShape>(btVector3(3.f, 3.f, 3.f)), trans));
}

void FirstRoom::Load(Level &previousLevel) {
	LOG(TLogLevel::logWARNING) << "This level should not receive a previous level.";
	Load();
}

void FirstRoom::Update(float deltaTime) {
	for each (sf::Event event in Game::events) {
		if (event.type == sf::Event::KeyReleased) {
			if (event.key.code == sf::Keyboard::C)
				world->SetCamera(cameras[++currentCamera % cameras.size()]);
		}
	}

	if (!middleTargetHit && middleTarget->WasHit()) {
		middleTargetHit = true;
		timer.restart();
	}
	else if (!rightTargetHit && rightTarget->WasHit()) {
		rightTargetHit = true;
		timer.restart();
	}
	else if (!leftTargetHit && leftTarget->WasHit()) {
		leftTargetHit = true;
		timer.restart();
	}
	
	float t = timer.getElapsedTime().asSeconds() > 2.f ? 2.f : timer.getElapsedTime().asSeconds();
	if (middleTargetHit && !rightTargetHit && !leftTargetHit) {
		lights[1]->SetDirection(btVector3(t * 5.f / 2.f, 3.f, -5.f));
	}
	else if (middleTargetHit && !leftTargetHit && rightTargetHit) {
		lights[1]->SetDirection(btVector3(t * (-10.f) / 2.f + 5.f, 3.f, -5.f));
	}
	else if (middleTargetHit && leftTargetHit && rightTargetHit) {
		if (t > 1.f && !levelDone) {
			GLfloat ambLight[] = { 0.5f, 0.5f, 0.5f, 1.f };
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambLight);

			lights[1]->Disable();
			lights[2]->Enable();
			lights[3]->Enable();
			lights[4]->Enable();

			door->SetActive(true);

			if (shared_ptr<CharacterEntity> charEnt = world->GetPlayer().lock()) {
				Dalek *dalek = dynamic_cast<Dalek *>(charEnt.get());
				dalek->ToggleMovement(true);
			}

			levelDone = true;
		}
	}

	camAngle += 8 * deltaTime;
	if (camAngle >= 360.f) camAngle = 0.f;
	cameras[1]->SetPosition(btVector3(cosf(DEG2RAD(camAngle)) * 10.f, 5, sinf(DEG2RAD(camAngle)) * 10.f));
}