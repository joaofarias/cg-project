#include "stdafx.h"
#include "AssetsManager.h"

#include "Renderer.h"
#include "ShapeImporter.h"

using namespace Poco::XML;

const string AssetsManager::assetTypes[] = {
	"dalekBody",
	"dalekEye",
	"dalekGun",
	"blueLaser",
	"sphere",
	"cube",
	"torus",
	"chessFloor",
	"ceiling",
	"wall",
	"structureTest",
	"firstRoom",
	"firstRoomWall",
	"door",
	"mirror",
	"target",
	"glass_2x2",
	"glassBroken_2x2",
	"glass_4x2",
	"glassBroken_4x2",
	"glass_2x6",
	"glassBroken_2x6",
	"glass_2x8",
	"glassBroken_2x8",
	"secondRoom",
	"secondRoomWall"
};

ShapeImporter AssetsManager::shapeImporter = ShapeImporter();

AssetsManager::AssetsManager()
{
	Poco::Path filePath = Poco::Path(ASSETS_FILE_DIRECTORY);

	Poco::File file = Poco::File(filePath);
	if (!file.exists()) {}
		file.createDirectories();

	filePath.setFileName(ASSETS_FILE_NAME);
	file = filePath;
	if (!file.exists())
		CreateXML(file);
	else
		LoadXML(file);
}

AssetsManager::~AssetsManager() { }

void AssetsManager::CreateXML(const Poco::File &file) {
	doc = new Document();
	AutoPtr<Element> root = doc->createElement("assets");
	doc->appendChild(root);

	DOMWriter writer;
	writer.setOptions(XMLWriter::PRETTY_PRINT | XMLWriter::WRITE_XML_DECLARATION);
	writer.writeNode(std::ofstream(file.path()), doc);
}

#include "Poco\Exception.h"
void AssetsManager::LoadXML(const Poco::File &file) {
	DOMParser parser;
	try {
		doc = parser.parse(file.path());
	}
	catch (Poco::Exception &exc) {
		LOG(TLogLevel::logERROR) << exc.displayText();
	}
}

void AssetsManager::CleanAssets() {
	for (auto it = renderers.begin(); it != renderers.end(); ) {
		if (it->second.unique())
			renderers.erase(it++);
		else it++;
	}

	for (auto it = shapes.begin(); it != shapes.end();) {
		if (it->second.unique()) {
			btCompoundShape *compound = dynamic_cast<btCompoundShape*>(it->second.get());
			if (compound) {
				compoundsChildShapes.erase(it->second.get());
			}
			shapes.erase(it++);
		}
			
		else it++;
	}
	
	for (auto it = sounds.begin(); it != sounds.end();) {
		if (it->second.size() > 0 && it->second[0].unique())
			sounds.erase(it++);
		else it++;
	}
}

/*
* 
*
*		Renderer
*
*
*/

shared_ptr<Renderer> AssetsManager::GetRenderer(AssetName name) {
	RendererHashPair pair = renderers.find(name);
	if (pair == renderers.end()) {
		string path = GetModelPath(assetTypes[name]);
		if (path != "") {
			shared_ptr<Renderer> renderer = std::make_shared<Renderer>(path);
			renderers.insert(std::make_pair(name, renderer));
			return renderer;
		}
		return nullptr;
	}
	else
		return pair->second;
}

string AssetsManager::GetModelPath(const string &id) {
	Element *entity = doc->getElementById(id, "id");
	if (entity) {
		entity = entity->getChildElement("model");
		if (entity)
			return entity->getAttribute("path");
	}
	return "";
}

/*
*
*
*		Shape
*
*
*/

shared_ptr<btCollisionShape> AssetsManager::GetShape(AssetName name, btVector3 &scale) {
	ShapeKey key = { name, scale };
	ShapeHashPair pair = shapes.find(key);
	if (pair == shapes.end()) {
		string path;
		if (GetShapePath(assetTypes[name], path)) {
			// load from file
			shared_ptr<btCollisionShape> shape;
			Poco::File filePath = Poco::File(path);
			if (!filePath.exists()) shape = CreateConvexHull(name);
			else shape = shared_ptr<btCollisionShape>(shapeImporter.GetShapeFromFile(path));
			
			shape->setLocalScaling(scale);
			key = { name, scale };
			shapes.insert(std::make_pair(key, shape));
			return shape;
		}
		else {
			// create new shape
			shared_ptr<btCollisionShape> newShape = CreateShape(name, path);
			if (newShape == nullptr)
				return newShape;

			newShape->setLocalScaling(scale);
			key = { name, scale };
			shapes.insert(std::make_pair(key, newShape));
				
			return newShape;
		}
	}
	else
		return pair->second;
}

// Returns true if pathOut is a path, false otherwise
bool AssetsManager::GetShapePath(const string &id, string &pathOut) {
	pathOut = "";
	Element *entity = doc->getElementById(id, "id");
	if (entity) {
		entity = entity->getChildElement("shape");
		if (entity) {
			pathOut = entity->getAttribute("path");
			if (pathOut != "")
				return true;
			pathOut = entity->getAttribute("primitive");
		}
	}
	return false;
}

void AssetsManager::AddShapePath(const string &id, const string &path) {
	Element *entity = doc->getElementById(id, "id");
	if (entity) {
		AutoPtr<Element> shapeEl;
		if ((shapeEl = entity->getChildElement("shape")))
			entity->removeChild(shapeEl);
		shapeEl = doc->createElement("shape");
		shapeEl->setAttribute("path", path);
		entity->appendChild(shapeEl);
		
		Poco::Path filePath = Poco::Path(ASSETS_FILE_DIRECTORY);
		filePath.setFileName(ASSETS_FILE_NAME);
				
		DOMWriter writer;
		writer.setOptions(XMLWriter::PRETTY_PRINT | XMLWriter::WRITE_XML_DECLARATION);
		writer.writeNode(std::ofstream(filePath.toString()), doc);
	}
}

shared_ptr<btCollisionShape> AssetsManager::CreateShape(AssetName name, const string &primitive) {
	shared_ptr<btCollisionShape> shape;
	if (primitive == "sphere")
		shape = std::make_shared<btSphereShape>(0.5f);
	else if (primitive == "cube")
		shape = std::make_shared<btBoxShape>(btVector3(0.5f, 0.5f, 0.5f));
	else if (primitive == "convex")
		shape = CreateConvexHull(name);
	else if (primitive == "breakable")
		shape = CreateBreakableCompound(name);
	else if (primitive == "")
		shape = CreateCompoundShape(name);
	return shape;
}

shared_ptr<btCollisionShape> AssetsManager::CreateConvexHull(AssetName name) {
	RendererHashPair pair = renderers.find(name);
	if (pair == renderers.end())
		return nullptr;
	shared_ptr<Renderer> renderer = pair->second;
	
	btAlignedObjectArray<btVector3> vertices;
	renderer->GetVerticesFromMesh(0, vertices);
	float* verticesf = new float[vertices.size() * 3];
	for (int i = 0; i < vertices.size(); i++) {
		verticesf[i * 3] = (float) vertices[i].x();
		verticesf[i * 3 + 1] = (float) vertices[i].y();
		verticesf[i * 3 + 2] = (float) vertices[i].z();
	}
	
	btConvexHullComputer convexHC = btConvexHullComputer();
	convexHC.compute(&verticesf[0], sizeof(float) * 3, vertices.size(), 0.0, 0.0);

	btConvexHullShape *convexHullShape = new btConvexHullShape(&(convexHC.vertices[0].getX()), convexHC.vertices.size());

	delete verticesf;

	btDefaultSerializer *serializer = new btDefaultSerializer();
	btCompoundShapeData *compoundShapeData = new btCompoundShapeData();

	serializer->startSerialization();
	convexHullShape->serializeSingleShape(serializer);
	serializer->finishSerialization();

	Poco::Path path = Poco::Path(SHAPE_FILE_DIRECTORY);

	Poco::File file = Poco::File(path);
	if (!file.exists())
		file.createDirectories();
	path.setFileName(assetTypes[name] + ".bcs");
	file = path;

	std::ofstream outStream = std::ofstream(file.path(), std::ofstream::binary);
	if (outStream.is_open()) {
		outStream.write((const char*) serializer->getBufferPointer(), serializer->getCurrentBufferSize());
		outStream.close();
	}

	AddShapePath(assetTypes[name], file.path());

	return shared_ptr<btConvexHullShape>(convexHullShape);
}

shared_ptr<btCollisionShape> AssetsManager::CreateBreakableCompound(AssetName name) {
	RendererHashPair pair = renderers.find(name);
	if (pair == renderers.end())
		return nullptr;
	shared_ptr<Renderer> renderer = pair->second;
	shared_ptr<btCompoundShape> compound = std::make_shared<btCompoundShape>();
	std::vector<unique_ptr<btCollisionShape>> childShapes;

	for (unsigned int i = 0; i < renderer->GetNumMeshes(); ++i) {
		btAlignedObjectArray<btVector3> vertices;
		renderer->GetVerticesFromMesh(i, vertices);
		
		float* verticesf = new float[vertices.size() * 3];
		for (int j = 0; j < vertices.size(); j++) {
			verticesf[j * 3] = (float) vertices[j].x();
			verticesf[j * 3 + 1] = (float) vertices[j].y();
			verticesf[j * 3 + 2] = (float) vertices[j].z();
		}

		btConvexHullComputer convexHC = btConvexHullComputer();
		convexHC.compute(&verticesf[0], sizeof(float) * 3, vertices.size(), 0.0f, 0.0f);
		
		// Calculate volume and center of mass (Stan Melax volume integration)
		int numFaces = convexHC.faces.size();
		int v0, v1, v2; // Triangle vertices
		btScalar volume = btScalar(0.);
		btVector3 com(0., 0., 0.);
		for (int j = 0; j < numFaces; j++) {
			const btConvexHullComputer::Edge* edge = &convexHC.edges[convexHC.faces[j]];
			v0 = edge->getSourceVertex();
			v1 = edge->getTargetVertex();
			edge = edge->getNextEdgeOfFace();
			v2 = edge->getTargetVertex();
			while (v2 != v0) {
				// Counter-clockwise triangulated voronoi shard mesh faces (v0-v1-v2) and edges here...
				btScalar vol = convexHC.vertices[v0].triple(convexHC.vertices[v1], convexHC.vertices[v2]);
				volume += vol;
				com += vol * (convexHC.vertices[v0] + convexHC.vertices[v1] + convexHC.vertices[v2]);
				edge = edge->getNextEdgeOfFace();
				v1 = v2;
				v2 = edge->getTargetVertex();
			}
		}
		if (volume == 0) continue;
		com /= volume * btScalar(4.);
		volume /= btScalar(6.);

		// Shift all vertices relative to center of mass
		int numVerts = convexHC.vertices.size();
		for (int j = 0; j < numVerts; j++)
		{
			convexHC.vertices[j] -= com;
		}
		
		btTransform transform;
		transform.setIdentity();
		transform.setOrigin(com);

		unique_ptr<btConvexHullShape> compoundChildShape = std::make_unique<btConvexHullShape>(&(convexHC.vertices[0].getX()), convexHC.vertices.size());
		compoundChildShape->setMargin(0.04f);
		compound->addChildShape(transform, compoundChildShape.get());
		childShapes.push_back(std::move(compoundChildShape));
		delete verticesf;
	}	

	compoundsChildShapes.insert(std::make_pair(compound.get(), std::move(childShapes)));

	btDefaultSerializer *serializer = new btDefaultSerializer();
	btCompoundShapeData *compoundShapeData = new btCompoundShapeData();

	serializer->startSerialization();
	compound->serializeSingleShape(serializer);
	serializer->finishSerialization();

	Poco::Path path = Poco::Path(SHAPE_FILE_DIRECTORY);

	Poco::File file = Poco::File(path);
	if (!file.exists())
		file.createDirectories();
	path.setFileName(assetTypes[name] + ".bcs");
	file = path;

	std::ofstream outStream = std::ofstream(file.path(), std::ofstream::binary);
	if (outStream.is_open()) {
		outStream.write((const char*) serializer->getBufferPointer(), serializer->getCurrentBufferSize());
		outStream.close();
	}

	AddShapePath(assetTypes[name], file.path());

	return compound;
}

shared_ptr<btCollisionShape> AssetsManager::CreateCompoundShape(AssetName name) {
	RendererHashPair pair = renderers.find(name);
	if (pair == renderers.end())
		return nullptr;
	shared_ptr<Renderer> renderer = pair->second;
	shared_ptr<btCompoundShape> compound = std::make_shared<btCompoundShape>();
	std::vector<unique_ptr<btCollisionShape>> childShapes;

	for (unsigned int i = 0; i < renderer->GetNumMeshes(); ++i) {
		btAlignedObjectArray<btVector3> vertices, triangleIndices;
		renderer->GetVerticesFromMesh(i, vertices, triangleIndices);

		HACD::HACD *hacd = CalcHACD(vertices, triangleIndices);

		LOG(TLogLevel::logDEBUG) << "clusters: " << hacd->GetNClusters();
		for (size_t i = 0; i < hacd->GetNClusters(); i++)
		{
			//generate convex result
			size_t nPoints = hacd->GetNPointsCH(i);
			size_t nTriangles = hacd->GetNTrianglesCH(i);

			float* vertices = new float[nPoints * 3];
			unsigned int* triangles = new unsigned int[nTriangles * 3];

			HACD::Vec3<HACD::Real> * pointsCH = new HACD::Vec3<HACD::Real>[nPoints];
			HACD::Vec3<long> * trianglesCH = new HACD::Vec3<long>[nTriangles];
			hacd->GetCH(i, pointsCH, trianglesCH);

			// points
			for (size_t v = 0; v < nPoints; v++)
			{
				vertices[3 * v] = (float) pointsCH[v].X();
				vertices[3 * v + 1] = (float) pointsCH[v].Y();
				vertices[3 * v + 2] = (float) pointsCH[v].Z();

			}
			// triangles
			for (size_t f = 0; f < nTriangles; f++)
			{
				triangles[3 * f] = trianglesCH[f].X();
				triangles[3 * f + 1] = trianglesCH[f].Y();
				triangles[3 * f + 2] = trianglesCH[f].Z();
			}

			delete[] pointsCH;
			delete[] trianglesCH;

			btConvexHullComputer convexHC = btConvexHullComputer();
			convexHC.compute(&vertices[0], sizeof(float) * 3, nPoints, 0.0, 0.0);

			btTransform transform;
			transform.setIdentity();
			unique_ptr<btConvexHullShape> compoundChildShape = std::make_unique<btConvexHullShape>(&(convexHC.vertices[0].getX()), convexHC.vertices.size());
			compoundChildShape->setMargin(0.04f);
			compound->addChildShape(transform, compoundChildShape.get());
			childShapes.push_back(std::move(compoundChildShape));
		}
		delete hacd;
	}

	compoundsChildShapes.insert(std::make_pair(compound.get(), std::move(childShapes)));
	
	btDefaultSerializer *serializer = new btDefaultSerializer();
	btCompoundShapeData *compoundShapeData = new btCompoundShapeData();

	serializer->startSerialization();
	compound->serializeSingleShape(serializer);
	serializer->finishSerialization();

	Poco::Path path = Poco::Path(SHAPE_FILE_DIRECTORY);

	Poco::File file = Poco::File(path);
	if (!file.exists())
		file.createDirectories();
	path.setFileName(assetTypes[name] + ".bcs");
	file = path;
	
	std::ofstream outStream = std::ofstream(file.path(), std::ofstream::binary);
	if (outStream.is_open()) {
		outStream.write((const char*)serializer->getBufferPointer(), serializer->getCurrentBufferSize());
		outStream.close();
	}

	AddShapePath(assetTypes[name], file.path());

	return compound;
}

HACD::HACD *AssetsManager::CalcHACD(const btAlignedObjectArray<btVector3> &vertices, const btAlignedObjectArray<btVector3> &triangleIndices) {

	std::vector< HACD::Vec3<HACD::Real> > points;
	std::vector< HACD::Vec3<long> > triangles;

	LOG(TLogLevel::logDEBUG) << "vertices: " << vertices.size();
	for (int i = 0; i<vertices.size(); i++)
	{
		HACD::Vec3<HACD::Real> vertex(vertices[i].x(), vertices[i].y(), vertices[i].z());
		points.push_back(vertex);
	}

	LOG(TLogLevel::logDEBUG) << "triangles: " << triangleIndices.size();
	for (int i = 0; i<triangleIndices.size(); i++)
	{
		HACD::Vec3<long> triangle((long) triangleIndices[i].x(), (long) triangleIndices[i].y(), (long) triangleIndices[i].z());
		triangles.push_back(triangle);
	}

	HACD::HACD *hacd = new HACD::HACD();
	hacd->SetPoints(&points[0]);
	hacd->SetNPoints(points.size());
	hacd->SetTriangles(&triangles[0]);
	hacd->SetNTriangles(triangles.size());

	// HACD parameters
	// Recommended parameters: 2 100 0 0 0 0
	size_t nClusters = 1;
	double concavity = 5;
	bool invert = false;
	bool addExtraDistPoints = true;
	bool addNeighboursDistPoints = true;
	bool addFacesPoints = true;

	// FIXME: this settings produce crashes with some models
	hacd->SetNClusters(nClusters);                     // minimum number of clusters
	hacd->SetNVerticesPerCH(100);                      // max of 100 vertices per convex-hull
	hacd->SetConcavity(concavity);                     // maximum concavity
	hacd->SetAddExtraDistPoints(addExtraDistPoints);
	hacd->SetAddNeighboursDistPoints(addNeighboursDistPoints);
	hacd->SetAddFacesPoints(addFacesPoints);

	hacd->Compute();

	return hacd;
}

/*
*
*
*		Sounds
*
*
*/

std::vector<shared_ptr<sf::SoundBuffer>> AssetsManager::GetSounds(AssetsManager::AssetName name) {
	SoundsHashPair pair = sounds.find(name);
	if (pair == sounds.end()) {
		int n = 1;
		string path;
		std::vector<shared_ptr<sf::SoundBuffer>> soundBuffers;
		while (GetSoundPath(assetTypes[name], n++, path)) {
			shared_ptr<sf::SoundBuffer> buffer = std::make_shared<sf::SoundBuffer>();
			if (!buffer->loadFromFile(path)) {
				LOG(TLogLevel::logWARNING) << "Unable to load sound " << n-1 << " from " << path;
				continue;
			}
			soundBuffers.push_back(buffer);
		}
		if (soundBuffers.size() > 0)
			sounds.insert(std::make_pair(name, soundBuffers));
		return soundBuffers;
	}
	else
		return pair->second;
}

bool AssetsManager::GetSoundPath(const string &id, int n, string &pathOut) {
	Element *entity = doc->getElementById(id, "id");
	if (entity) {
		entity = entity->getChildElement("sound_" + std::to_string(n));
		if (entity) {
			pathOut = entity->getAttribute("path");
			return true;
		}
	}
	return false;
}