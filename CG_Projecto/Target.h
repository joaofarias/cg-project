#pragma once
#include "BreakableEntity.h"
class Target :
	public BreakableEntity
{
public:
	Target(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass = 1.f, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Target();

	virtual void Update(float deltaTime);
	virtual void OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint);
	bool WasHit();

	virtual inline string GetDescription() { return "Target"; }

private:
	bool hit;
};

