#include "stdafx.h"
#include "MirrorCube.h"

#include "PhysicsWorld.h"
#include "Renderer.h"

MirrorCube::MirrorCube(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
: DynamicEntity(world, name, mass, transform, scale)
{
	rigidBody->setRollingFriction(0.5f);
	rigidBody->setFriction(0.5f);
	rigidBody->setDamping(0.5f, 0.75f);
	rigidBody->setSleepingThresholds(0.2f, 0.2f);

	for (int i = 0; i < 5; i++) {
		dynamicsWorld->AddEntity(std::make_shared<MirrorCubeFace>(world, renderer, i, motionState, nullptr, sounds, scale));
	}
	dynamicsWorld->AddEntity(std::make_shared<MirrorCubeFace>(world, renderer, 5, motionState, nullptr, sounds, scale), true, true);
	
}

MirrorCube::~MirrorCube() { }

MirrorCubeFace::MirrorCubeFace(shared_ptr<PhysicsWorld> world, shared_ptr<Renderer> renderer, unsigned int rendererMeshId, shared_ptr<btMotionState> motionState, unique_ptr<btRigidBody> rigidBody, std::vector<sf::Sound> sounds, btVector3 &scale)
: DynamicEntity(world, renderer, rendererMeshId, motionState, std::move(rigidBody), sounds)
{
	this->scale = scale;	
}

void MirrorCubeFace::Draw(float deltaTime, bool lighting, bool cullface) {
	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glPushMatrix();
		glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
		glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
		glScalef(scale.x(), scale.y(), scale.z());

		if (rendererMeshID < 0)	renderer->Draw(lighting, cullface);
		else					renderer->Draw(rendererMeshID, lighting, cullface);
	glPopMatrix();
	glDisable(GL_NORMALIZE);
}

void MirrorCubeFace::RemoveFromWorld() {
	readyToDie = true;
}

btVector3 MirrorCubeFace::GetPosition() {
	if (motionState == nullptr) return btVector3(0, 0, 0);
	btTransform t;
	motionState->getWorldTransform(t);
	switch (rendererMeshID) {
		case 0:
			return t.getOrigin() + (btVector3(0, 0.5f, 0)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		case 1:
			return t.getOrigin() + (btVector3(0, -0.5f, 0)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		case 2:
			return t.getOrigin() + (btVector3(-0.5f, 0, 0)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		case 3:
			return t.getOrigin() + (btVector3(0.5f, 0, 0)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		case 4:
			return t.getOrigin() + (btVector3(0, 0, 0.5f)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		case 5:
			return t.getOrigin() + (btVector3(0, 0, -.5f)*scale).rotate(t.getRotation().getAxis(), t.getRotation().getAngle());
		default:
			return t.getOrigin();
	}
}

btQuaternion MirrorCubeFace::GetRotation() {
	if (motionState == nullptr) return btQuaternion(0, 0, 0);
	btTransform t;
	motionState->getWorldTransform(t);

	switch (rendererMeshID) {
		case 0:
			return t.getRotation() + btQuaternion(0, AI_MATH_HALF_PI_F, 0);
		case 1:
			return t.getRotation() + btQuaternion(0, -AI_MATH_HALF_PI_F, 0);
		case 2:
			return t.getRotation() + btQuaternion(-AI_MATH_HALF_PI_F, 0, 0);
		case 3:
			return t.getRotation() + btQuaternion(AI_MATH_HALF_PI_F, 0, 0);
		case 4:
			t.getRotation().setRotation((t.getRotation().getAxis() + btVector3(0, 1, 0)).normalized(), t.getRotation().getAngle() + AI_MATH_PI_F);
			return t.getRotation();
		default:
			return t.getRotation();
	}
}

btVector3 MirrorCubeFace::GetForwardVector() {
	if (motionState == nullptr) return btVector3(0, 0, 1);
	btTransform t;
	motionState->getWorldTransform(t);

	switch (rendererMeshID) {
		case 0:
			return t.getBasis().getColumn(2).rotate(btVector3(1, 0, 0), AI_MATH_HALF_PI_F);
		case 1:
			return t.getBasis().getColumn(2).rotate(btVector3(1, 0, 0), -AI_MATH_HALF_PI_F);
		case 2:
			return t.getBasis().getColumn(2).rotate(btVector3(0, 1, 0), -AI_MATH_HALF_PI_F);
		case 3:
			return t.getBasis().getColumn(2).rotate(btVector3(0, 1, 0), AI_MATH_HALF_PI_F);
		case 4:
			return t.getBasis().getColumn(2).rotate(btVector3(0, 1, 0), AI_MATH_PI_F);
		default:
			return t.getBasis().getColumn(2);
	}
}

btVector3 MirrorCubeFace::GetReflectionAxis()
{
	switch (rendererMeshID) {
		case 0:
			return btVector3(1, -1, 1);
		case 1:
			return btVector3(1, -1, 1);
		case 2:
			return btVector3(-1, 1, 1);
		case 3:
			return btVector3(-1, 1, 1);
		case 4:
			return btVector3(1, 1, -1);
		default:
			return btVector3(1, 1, -1);
	}
}