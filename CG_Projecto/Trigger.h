#pragma once

class PhysicsWorld;

class Trigger
{
public:
	Trigger(shared_ptr<PhysicsWorld> world, unique_ptr<btCollisionShape> shape, btTransform &trans);
	~Trigger();

	bool TriggeredBy(btCollisionObject *obj);

private:
	shared_ptr<PhysicsWorld> world;
	unique_ptr<btCollisionShape> shape;
	unique_ptr<btCollisionObject> trigger;

	struct LevelTriggerTest : btCollisionWorld::ContactResultCallback {
		bool trigger;
		LevelTriggerTest() : trigger(false) {};
		virtual btScalar addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1);
	};
};

