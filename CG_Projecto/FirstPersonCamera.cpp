#include "stdafx.h"
#include "FirstPersonCamera.h"
#include "Game.h"
#include "CharacterEntity.h"
#include "HUD.h"

FirstPersonCamera::FirstPersonCamera()
	: FirstPersonCamera(90, (float)Game::GetScreenWidth() / Game::GetScreenHeight(), 0.1, 1000) {}

FirstPersonCamera::FirstPersonCamera(GLdouble fov, GLdouble ratio, GLdouble zNear, GLdouble zFar)
	: Camera(fov, ratio, zNear, zFar, btVector3(0, 0, 0), btVector3(0, -1, 0), btVector3(0, 1, 0)) {
}

FirstPersonCamera::~FirstPersonCamera() { }

void FirstPersonCamera::Update(float deltaTime) {
	shared_ptr<CharacterEntity> charEnt = charEntity.lock();
	if (!charEnt) return;

	btTransform transform;
	charEnt->motionState->getWorldTransform(transform);
	SetPosition(transform.getOrigin() + charEnt->viewPosition);
	SetTarget(transform.getOrigin() + charEnt->viewPosition + charEnt->viewDirection);

	if (hud != nullptr)
		hud->Update(deltaTime);
}

void FirstPersonCamera::SetCharacterEntity(weak_ptr<CharacterEntity> charEntity) {
	this->charEntity = charEntity;
}