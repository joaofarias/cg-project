#include "stdafx.h"
#include "BrokenGlass.h"

#include "PhysicsWorld.h"

BrokenGlass::BrokenGlass(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
	: BreakableEntity(world, name, true, false, mass, transform, scale)
{
	for (int i = 0; i < rigidBodies.size(); ++i) {
		rigidBodies[i]->setRollingFriction(0.5f);
		rigidBodies[i]->setDamping(0.1f, 0.75f);
	}
}

BrokenGlass::~BrokenGlass() {}

void BrokenGlass::ToDynamic(btRigidBody *rb) {
	btVector3 inertia;
	dynamicsWorld->GetWorld()->removeRigidBody(rb);
	if (mass > 0)
		rb->getCollisionShape()->calculateLocalInertia(mass, inertia);
	rb->setMassProps(mass, inertia);

	for (unsigned int i = 0; i < rigidBodies.size(); ++i) {
		if (rigidBodies[i] == nullptr || rigidBodies[i].get() != rb) continue;
		entities.push_back(std::make_shared<GlassShard>(dynamicsWorld, renderer, i, motionStates[i], std::move(rigidBodies[i]), sounds));
		dynamicsWorld->AddEntity(entities.back(), transparent, reflective);
		timeInactive.push_back(0.f);
		break;
	}
}