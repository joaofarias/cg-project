#pragma once

class Renderer
{
public:
	Renderer(string modelName);
	~Renderer();

	void ExportModel();
	void Draw(unsigned int mesh, bool lighting = true, bool cullFace = true);
	void Draw(bool lighting = true, bool cullFace = true);
	unsigned int GetNumMeshes();
	void GetVerticesFromMesh(unsigned int meshIndex, btAlignedObjectArray<btVector3> &vertices);
	void GetVerticesFromMesh(unsigned int meshIndex, btAlignedObjectArray<btVector3> &vertices, btAlignedObjectArray<btVector3> &triangleIndices);
	void CenterOnOrigin(unsigned int meshIndex, btTransform &com);
	string GetModelPath();
private:
	Assimp::Importer importer;
	
	string modelName;
	const aiScene *scene;
	std::map<std::string, GLuint*> textureIdMap;	// map image filenames to textureIds
	GLuint*	textureIds;

	void ImportModel();
	bool LoadGLTextures();
	void Render(const aiNode *node, bool lighting, bool cullFace);
	void ApplyMaterial(const aiMaterial *mtl, bool cullFace);
	


	inline void Color4f(const aiColor4D *color) {
		glColor4f(color->r, color->g, color->b, color->a);
	}

	inline void FillColorArray(float color[4], float a, float b, float c, float d)
	{
		color[0] = a;
		color[1] = b;
		color[2] = c;
		color[3] = d;
	}

	inline void Color4DToFloatArray(const aiColor4D *c4D, float fa[4])
	{
		fa[0] = c4D->r;
		fa[1] = c4D->g;
		fa[2] = c4D->b;
		fa[3] = c4D->a;
	}
};

