#pragma once
#include "StaticEntity.h"

class Trigger;

class Door :
	public StaticEntity
{
public:
	Door(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, bool enabled = true, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Door();

	virtual void Update(float deltaTime);
	void SetActive(bool active);

private:
	unique_ptr<Trigger> doorSensor;
	bool opening;
	bool enabled;
};

