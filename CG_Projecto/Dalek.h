#pragma once
#include "CharacterEntity.h"

class Renderer;

class Dalek :
	public CharacterEntity
{
public:
	Dalek(shared_ptr<PhysicsWorld> world, btScalar mass = 1, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Dalek();

	virtual void Draw(float deltaTime, bool lighting = true, bool cullface = true);
	virtual void Update(float deltaTime);

	void ToggleLook(bool enabled);
	void ToggleMovement(bool enabled);
	void ToggleFiring(bool enabled);
private:
	shared_ptr<Renderer> eyeRenderer;
	shared_ptr<btCollisionShape> eyeShape;
	btTransform eyeTransform;

	shared_ptr<Renderer> gunRenderer;
	shared_ptr<btCollisionShape> gunShape;
	btTransform gunTransform;

	bool lookEnabled, movementEnabled, firingEnabled;

};

