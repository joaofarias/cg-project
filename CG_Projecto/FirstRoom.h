#pragma once
#include "Level.h"
#include "SFML\Graphics.hpp"

class PhysicsWorld;
class Camera;
class SecondRoom;
class Target;
class Light;
class Door;

class FirstRoom :
	public Level
{
	friend SecondRoom;
public:
	FirstRoom(std::shared_ptr<PhysicsWorld> world);
	~FirstRoom();

	virtual void Load();
	virtual void Load(Level &previousLevel);
	virtual void Update(float deltaTime);

private:
	sf::Clock timer;
	bool leftTargetHit, middleTargetHit, rightTargetHit;
	bool levelDone;
	float camAngle;

	std::vector<shared_ptr<Camera>> cameras;
	int currentCamera;

	std::vector<shared_ptr<Light>> lights;
	shared_ptr<Target> leftTarget;
	shared_ptr<Target> middleTarget;
	shared_ptr<Target> rightTarget;
	shared_ptr<Door> door;
};

