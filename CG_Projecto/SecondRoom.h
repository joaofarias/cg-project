#pragma once
#include "Level.h"

class PhysicsWorld;
class Camera;

class SecondRoom :
	public Level
{
public:
	SecondRoom(std::shared_ptr<PhysicsWorld> world);
	~SecondRoom();

	virtual void Load();
	virtual void Load(Level &previousLevel);
	virtual void Update(float deltaTime);

private:
	std::vector<shared_ptr<Camera>> cameras;
	int currentCamera;
	float camAngle;
};

