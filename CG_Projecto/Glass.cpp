#include "stdafx.h"
#include "Glass.h"

#include "PhysicsWorld.h"
#include "BrokenGlass.h"
#include "Wall.h"

Glass::Glass(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btTransform &transform, btVector3 &scale)
	: StaticEntity(world, name, transform, scale)
{
	// just loading these to memory so they are already loaded when needed
	switch (name) {
		case AssetsManager::AssetName::Glass_2x2:
			brokenAssetName = AssetsManager::AssetName::GlassBroken_2x2;
			break;
		case AssetsManager::AssetName::Glass_4x2:
			brokenAssetName = AssetsManager::AssetName::GlassBroken_4x2;
			break;
		case AssetsManager::AssetName::Glass_2x6:
			brokenAssetName = AssetsManager::AssetName::GlassBroken_2x6;
			break;
		case AssetsManager::AssetName::Glass_2x8:
			brokenAssetName = AssetsManager::AssetName::GlassBroken_2x8;
			break;
		default:
			brokenAssetName = name;
	}
	assetsManager.GetRenderer(brokenAssetName);
	assetsManager.GetShape(brokenAssetName, scale);
	assetsManager.GetSounds(brokenAssetName);

	collisionCallback = true;
	broken = false;
	checkStructure = true;
}

Glass::~Glass() {}

void Glass::Update(float deltaTime) {
	if (checkStructure) {
		checkStructure = false;

		CollisionTestResult result;
		result.testingObject = rigidBody.get();
		dynamicsWorld->GetWorld()->contactTest(rigidBody.get(), result);
		if (!result.ok) Break();
	}
}

void Glass::Break() {
	broken = true;

	btTransform trans;
	motionState->getWorldTransform(trans);
	shared_ptr<BrokenGlass> brokenGlass = std::make_shared<BrokenGlass>(dynamicsWorld, brokenAssetName, 50.f, trans, (btVector3) shape->getLocalScaling());
	dynamicsWorld->AddEntity(brokenGlass, true);
}

void Glass::CheckStructure() {
	checkStructure = true;
}

void Glass::OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint) {
	if (broken) return;

	this->RemoveFromWorld();

	Break();
}

btScalar Glass::CollisionTestResult::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1) {
	if (cp.getDistance() > 0.001f) return 0;

	btRigidBody *other = nullptr;
	if (colObj0Wrap->getCollisionObject() != testingObject)
		other = (btRigidBody *) btRigidBody::upcast(colObj0Wrap->getCollisionObject());
	else if (colObj1Wrap->getCollisionObject() != testingObject)
		other = (btRigidBody *) btRigidBody::upcast(colObj1Wrap->getCollisionObject());
	if (other == nullptr) return 0;

	Wall *wall = dynamic_cast<Wall *>((Entity *) other->getUserPointer());
	if (wall) ok = true;

	return cp.getDistance();
}
