#include "stdafx.h"
#include "BreakableEntity.h"

#include "Renderer.h"
#include "PhysicsWorld.h"
#include "DynamicEntity.h"

BreakableEntity::BreakableEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btScalar mass, btTransform &transform, btVector3 &scale)
	: BreakableEntity(world, name, false, false, mass, transform, scale)
{}

BreakableEntity::BreakableEntity(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, bool transparent, bool reflective, btScalar mass, btTransform &transform, btVector3 &scale)
	: Entity(world, name)
{
	this->mass = mass;

	renderer = std::make_shared<Renderer>(Entity::renderer->GetModelPath());
	motionState = std::make_shared<btDefaultMotionState>(transform);

	btCompoundShape *compound = dynamic_cast<btCompoundShape*>(shape.get());
	if (!compound) return;

	btVector3 inertia(0, 0, 0);
	btTransform t;
	for (int i = 0; i < compound->getNumChildShapes(); ++i) {
		renderer->CenterOnOrigin(i, compound->getChildTransform(i));
		
		compound->getChildShape(i)->setLocalScaling(scale);
		t.setOrigin((compound->getChildTransform(i).getOrigin()*scale).rotate(transform.getRotation().getAxis(), transform.getRotation().getAngle()) + transform.getOrigin());
		t.setRotation(transform.getRotation());
		motionStates.push_back(std::make_shared<btDefaultMotionState>(t));
		btRigidBody::btRigidBodyConstructionInfo rbInfo(0.f, motionStates.back().get(), compound->getChildShape(i), inertia);
		rigidBodies.push_back(std::make_unique<btRigidBody>(rbInfo));
		rigidBodies.back()->setUserPointer(this);
		dynamicsWorld->GetWorld()->addRigidBody(rigidBodies.back().get());
	}

	timeToDisappear = 3.f;

	checkStructure = true;

	collisionCallback = true;

	this->transparent = transparent;
	this->reflective = reflective;
	
	canPlaySound = true;
}

BreakableEntity::~BreakableEntity() {}

void BreakableEntity::Draw(float deltaTime, bool lighting, bool cullface) {
	btTransform trans;
	btVector3 translation;
	btQuaternion rotation;
	btVector3 scale;

	btCompoundShape *compound = dynamic_cast<btCompoundShape*>(shape.get());
	if (!compound) return;
	
	for (unsigned int i = 0; i < rigidBodies.size(); ++i) {
		if (rigidBodies[i] == nullptr) continue;

		motionStates[i]->getWorldTransform(trans);
		translation = trans.getOrigin();
		rotation = trans.getRotation().normalized();
		scale = rigidBodies[i]->getCollisionShape()->getLocalScaling();
		if (scale != btVector3(1, 1, 1))
			glEnable(GL_NORMALIZE);

		glPushMatrix();
			glTranslatef(translation.getX(), translation.getY(), translation.getZ());
			glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
			glScalef(scale.x(), scale.y(), scale.z());

			renderer->Draw(i, lighting, false);
		glPopMatrix();
		glDisable(GL_NORMALIZE);
	}
}


void BreakableEntity::Update(float deltaTime) {
	bool tooManyEntities = dynamicsWorld->GetNumActiveObjects() > 500;
	for (int i = 0; i < entities.size(); ++i) {
		btRigidBody *rb = entities[i]->GetRigidBody();
		if (!rb->isActive() || tooManyEntities) {
			timeInactive[i] += deltaTime;
			if (timeInactive[i] >= timeToDisappear || tooManyEntities) {
				entities[i]->RemoveFromWorld();
				entities.erase(entities.begin() + i);
				timeInactive.erase(timeInactive.begin() + i);
			}
		}
		else if (rb->isActive() && timeInactive[i] > 0.f) {
			timeInactive[i] = 0.f;
		}
	}

	if (checkStructure) {
		attachedRBs.clear();
		
		for (int i = 0; i < rigidBodies.size(); ++i) {
			if (rigidBodies[i] == nullptr) continue;

			if (!IsAttached(rigidBodies[i].get(), rigidBodies[i].get()))
				ToDynamic(rigidBodies[i].get());
		}

		checkStructure = false;
	}

	canPlaySound = true;
}

void BreakableEntity::RemoveFromWorld() {
	for (int i = 0; i < rigidBodies.size(); ++i) {
		if (rigidBodies[i])
			dynamicsWorld->GetWorld()->removeRigidBody(rigidBodies[i].get());
	}
	readyToDie = true;
}

void BreakableEntity::CheckStructure() {
	checkStructure = true;
}

void BreakableEntity::OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint) {
	if (!victim->isStaticOrKinematicObject()) return;

	ToDynamic(victim);

	checkStructure = true;

	if (canPlaySound && sounds.size() > 0) {
		for (int i = 0; i < sounds.size(); ++i) {
			if (sounds[i].getStatus() != sf::Sound::Playing) {
				sounds[i].play();
				canPlaySound = false;
				return;
			}
		}
		sounds.push_back(sf::Sound(*((sf::SoundBuffer*)sounds[0].getBuffer())));
		sounds.back().play();
		canPlaySound = false;
		return;
	}
}

void BreakableEntity::ToDynamic(btRigidBody *rb) {
	btVector3 inertia;
	dynamicsWorld->GetWorld()->removeRigidBody(rb);
	if (mass > 0)
		rb->getCollisionShape()->calculateLocalInertia(mass, inertia);
	rb->setMassProps(mass, inertia);
	
	for (unsigned int i = 0; i < rigidBodies.size(); ++i) {
		if (rigidBodies[i] == nullptr || rigidBodies[i].get() != rb) continue;
		entities.push_back(std::make_shared<DynamicEntity>(dynamicsWorld, renderer, i, motionStates[i], std::move(rigidBodies[i]), sounds));
		dynamicsWorld->AddEntity(entities.back(), transparent, reflective);
		timeInactive.push_back(0.f);
		break;
	}
}

bool BreakableEntity::IsAttached(btRigidBody *rb, btRigidBody *from) {
	HashPair pair = attachedRBs.find(rb);
	if (pair != attachedRBs.end())
		return pair->second;

	attachedRBs[rb] = false;

	CollisionTestResult ctResult;
	ctResult.testingObject = rb;
	dynamicsWorld->GetWorld()->contactTest(rb, ctResult);
	
	for (int i = 0; i < ctResult.collidedObjects.size(); ++i) {
		if (!ctResult.collidedObjects[i]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

		pair = attachedRBs.find(ctResult.collidedObjects[i]);
		if (ctResult.collidedObjects[i]->getUserPointer() != this || (pair != attachedRBs.end() && pair->second)) {
			attachedRBs[rb] = true;
			for (int j = 0; j < ctResult.collidedObjects.size(); ++j) {
				if (!ctResult.collidedObjects[j]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

				attachedRBs[ctResult.collidedObjects[j]] = true;
			}
			return true;
		}
	}
	
	for (int i = 0; i < ctResult.collidedObjects.size(); ++i) {
		if (!ctResult.collidedObjects[i]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;

		if (IsAttached(ctResult.collidedObjects[i], rb)) {
			attachedRBs[rb] = true;
			for (int j = 0; j < ctResult.collidedObjects.size(); ++j) {
				if (!ctResult.collidedObjects[j]->isStaticOrKinematicObject() || ctResult.collidedObjects[i] == from) continue;
				
				attachedRBs[ctResult.collidedObjects[j]] = true;
			}
			return true;
		}
		
	}
	
	return false;
}

btScalar BreakableEntity::CollisionTestResult::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1) {
	if (cp.getDistance() > 0.001f) return 0;

	btRigidBody *other = nullptr;
	if (colObj0Wrap->getCollisionObject() != testingObject)
		other = (btRigidBody *) btRigidBody::upcast(colObj0Wrap->getCollisionObject());
	else if (colObj1Wrap->getCollisionObject() != testingObject)
		other = (btRigidBody *) btRigidBody::upcast(colObj1Wrap->getCollisionObject());
	if (other == nullptr) return 0;

	if (collidedObjects.findLinearSearch(other) == collidedObjects.size())
		collidedObjects.push_back(other);

	return cp.getDistance();
}

