#pragma once

#include <SFML\Graphics.hpp>

class PhysicsWorld;
class Camera;
class HUD;
class CharacterEntity;
class Light;
class Entity;

int main();

class Game {

public:
	static std::list<const sf::Event> events;

	Game();
	~Game();
	
	void RunGame();

	static int GetScreenWidth();
	static int GetScreenHeight();
	static void DrawString(const sf::Text str);

private:
	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static std::list<const sf::Text> strings;

	sf::RenderWindow window;
	sf::VideoMode videoMode;
	bool gameRunning;
	sf::Clock gameClock;

	shared_ptr<PhysicsWorld> physicsWorld;

	void Draw(float deltaTime);
	void Update(float deltaTime);

};