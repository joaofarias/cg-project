#pragma once
#include "StaticEntity.h"
class Glass :
	public StaticEntity
{
public:
	Glass(shared_ptr<PhysicsWorld> world, AssetsManager::AssetName name, btTransform &transform = btTransform(btQuaternion(0, 0, 0), btVector3(0, 0, 0)), btVector3 &scale = btVector3(1, 1, 1));
	~Glass();

	virtual void Update(float deltaTime);
	virtual void OnExplosion(btRigidBody *exploder, btRigidBody *victim, btManifoldPoint &contactPoint);
	virtual void CheckStructure();

	virtual inline string GetDescription() { return "Glass"; }

private:
	bool broken;
	bool checkStructure;
	AssetsManager::AssetName brokenAssetName;

	void Break();

	struct CollisionTestResult : btCollisionWorld::ContactResultCallback {
		btRigidBody *testingObject;
		bool ok = false;
		virtual btScalar addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1);
	};
};

