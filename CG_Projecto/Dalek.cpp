#include "stdafx.h"
#include "Dalek.h"

#include "Game.h"
#include "Renderer.h"
#include "PhysicsWorld.h"
#include "Laser.h"

#define EYE_ORIGIN	0.0f, 1.529f, -0.258f	
#define EYE_LENGTH	0.375f
#define GUN_ORIGIN	0.19f, 0.946f, -0.113f
#define GUN_LENGTH	0.45f
#define LIMIT_UP	55.f
#define LIMIT_DOWN	-10.f

#define CONTROLS_FORWARD			sf::Keyboard::isKeyPressed(sf::Keyboard::W)
#define CONTROLS_BACKWARD			sf::Keyboard::isKeyPressed(sf::Keyboard::S)
#define CONTROLS_STRAFE_RIGHT		sf::Keyboard::isKeyPressed(sf::Keyboard::D)
#define CONTROLS_STRAFE_LEFT		sf::Keyboard::isKeyPressed(sf::Keyboard::A)
#define CONTROLS_FIRE				sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)
#define CONTROLS_HOVER				sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)

Dalek::Dalek(shared_ptr<PhysicsWorld> world, btScalar mass, btTransform &transform, btVector3 &scale)
	:CharacterEntity(world, AssetsManager::DalekBody, mass, btVector3(0, 0, 0), transform, scale) {

	btCompoundShape *compound = dynamic_cast<btCompoundShape*>(shape.get());
	if (!compound) return;

	eyeRenderer = assetsManager.GetRenderer(AssetsManager::DalekEye);
	eyeShape = assetsManager.GetShape(AssetsManager::DalekEye, scale);
	eyeTransform.setIdentity();
	eyeTransform.setOrigin(btVector3(EYE_ORIGIN) * scale);
	compound->addChildShape(eyeTransform, eyeShape.get());

	gunRenderer = assetsManager.GetRenderer(AssetsManager::DalekGun);
	gunShape = assetsManager.GetShape(AssetsManager::DalekGun, scale);
	gunTransform.setIdentity();
	gunTransform.setOrigin(btVector3(GUN_ORIGIN) * scale);
	compound->addChildShape(gunTransform, gunShape.get());

	dynamicsWorld->GetWorld()->removeRigidBody(rigidBody.get());
	dynamicsWorld->GetWorld()->addRigidBody(rigidBody.get());

	viewDirection = eyeTransform.getBasis().getColumn(2);
	viewDirection = viewDirection.rotate(btVector3(0, 1, 0), DEG2RAD(totalAngleY));
	viewDirection.normalize();

	viewPosition = eyeTransform.getOrigin().rotate(transform.getRotation().getAxis(), transform.getRotation().getAngle()) + viewDirection * EYE_LENGTH;

	speed = 5000;
	fireRate = 0.3f;

	lookEnabled = movementEnabled = firingEnabled = false;
}

Dalek::~Dalek() {}

void Dalek::Draw(float deltaTime, bool lighting, bool cullface) {
	if (rigidBody == nullptr) return;

	btTransform transform;
	motionState->getWorldTransform(transform);
	btQuaternion rotation = transform.getRotation().normalized();
	const btVector3 scale = rigidBody->getCollisionShape()->getLocalScaling();
	if (scale != btVector3(1, 1, 1))
		glEnable(GL_NORMALIZE);

	glPushMatrix();
		glTranslatef(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ());
		glRotatef(RAD2DEG(rotation.getAngle()), rotation.getX(), rotation.getY(), rotation.getZ());
		glPushMatrix();
			glScalef(scale.x(), scale.y(), scale.z());
			renderer->Draw(lighting, cullface);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(eyeTransform.getOrigin().getX(), eyeTransform.getOrigin().getY(), eyeTransform.getOrigin().getZ());
			glRotatef(RAD2DEG(eyeTransform.getRotation().getAngle()), eyeTransform.getRotation().getX(), eyeTransform.getRotation().getY(), eyeTransform.getRotation().getZ());
			glScalef(scale.x(), scale.y(), scale.z());
			eyeRenderer->Draw(lighting, cullface);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(gunTransform.getOrigin().getX(), gunTransform.getOrigin().getY(), gunTransform.getOrigin().getZ());
			glRotatef(RAD2DEG(gunTransform.getRotation().getAngle()), gunTransform.getRotation().getX(), gunTransform.getRotation().getY(), gunTransform.getRotation().getZ());
			glScalef(scale.x(), scale.y(), scale.z());
			gunRenderer->Draw(lighting, cullface);
		glPopMatrix();
	glPopMatrix();
	glDisable(GL_NORMALIZE);

}

void Dalek::Update(float deltaTime) {
	btCompoundShape *compound = dynamic_cast<btCompoundShape*>(shape.get());
	if (!compound) return;
	
	btTransform transform;
	rigidBody->getMotionState()->getWorldTransform(transform);
	
	if (lookEnabled) {
		int mouseX = sf::Mouse::getPosition().x;
		int mouseY = sf::Mouse::getPosition().y;

		totalAngleY -= (mouseX - Game::GetScreenWidth() / 2) * 4 * deltaTime;
		if (totalAngleY > 360.f) totalAngleY -= 360;
		else if (totalAngleY < -360.f) totalAngleY += 360.f;

		totalAngleX += (Game::GetScreenHeight() / 2 - mouseY) * 4 * deltaTime;
		if (totalAngleX > LIMIT_UP) totalAngleX = LIMIT_UP;
		else if (totalAngleX < LIMIT_DOWN) totalAngleX = LIMIT_DOWN;
	
		eyeTransform.setRotation(btQuaternion(0, DEG2RAD(-totalAngleX), 0));
		compound->removeChildShape(eyeShape.get());
		compound->addChildShape(eyeTransform, eyeShape.get());

		gunTransform.setRotation(btQuaternion(0, DEG2RAD(-totalAngleX), 0));
		compound->removeChildShape(gunShape.get());
		compound->addChildShape(gunTransform, gunShape.get());

		dynamicsWorld->GetWorld()->removeRigidBody(rigidBody.get());
		dynamicsWorld->GetWorld()->addRigidBody(rigidBody.get());

		transform.setRotation(btQuaternion(DEG2RAD(totalAngleY), 0, 0));
		rigidBody->getMotionState()->setWorldTransform(transform);
		rigidBody->setCenterOfMassTransform(transform);

		viewDirection = eyeTransform.getBasis().getColumn(2);
		viewDirection = viewDirection.rotate(btVector3(0, 1, 0), DEG2RAD(totalAngleY));
		viewDirection.normalize();

		viewPosition = eyeTransform.getOrigin().rotate(transform.getRotation().getAxis(), transform.getRotation().getAngle()) + viewDirection * EYE_LENGTH;
	}

	if (movementEnabled) {
		btVector3 dir = transform.getBasis().getColumn(2).normalized();
		if (CONTROLS_FORWARD && !CONTROLS_BACKWARD) {
			if (CONTROLS_STRAFE_RIGHT && !CONTROLS_STRAFE_LEFT)
				dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(-45)).normalized();
			else if (CONTROLS_STRAFE_LEFT && !CONTROLS_STRAFE_RIGHT)
				dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(45)).normalized();
			rigidBody->applyCentralImpulse(dir * speed * deltaTime);
		}
		else if (CONTROLS_BACKWARD && !CONTROLS_FORWARD) {
			if (CONTROLS_STRAFE_RIGHT && !CONTROLS_STRAFE_LEFT)
				dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(-135)).normalized();
			else if (CONTROLS_STRAFE_LEFT && !CONTROLS_STRAFE_RIGHT)
				dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(135)).normalized();
			else
				dir = -dir;
			rigidBody->applyCentralImpulse(dir * speed * deltaTime);
		}
		else if (CONTROLS_STRAFE_RIGHT && !CONTROLS_STRAFE_LEFT) {
			dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(-90)).normalized();
			rigidBody->applyCentralImpulse(dir * speed * deltaTime);
		}
		else if (CONTROLS_STRAFE_LEFT && !CONTROLS_STRAFE_RIGHT) {
			dir = dir.rotate(btVector3(0, 1, 0), DEG2RAD(90)).normalized();
			rigidBody->applyCentralImpulse(dir * speed * deltaTime);
		}
	}

	if (firingEnabled &&  CONTROLS_FIRE && clock.getElapsedTime().asSeconds() > fireRate) {
		btVector3 gunDirection = gunTransform.getBasis().getColumn(2).normalized();
		gunDirection = gunDirection.rotate(btVector3(0, 1, 0), DEG2RAD(totalAngleY));
		btVector3 gunPosition = gunTransform.getOrigin().rotate(transform.getRotation().getAxis(), transform.getRotation().getAngle()) + gunDirection * GUN_LENGTH;
		
		btTransform projTransf;
		projTransf.setIdentity();
		projTransf.setOrigin(transform.getOrigin() + gunPosition);
		projTransf.setRotation(btQuaternion(DEG2RAD(totalAngleY), DEG2RAD(-totalAngleX), 0));
		unique_ptr<Laser> projectile = std::make_unique<Laser>(dynamicsWorld, projTransf);
		dynamicsWorld->AddEntity(std::move(projectile), true);
		clock.restart();
	}

	if (CONTROLS_HOVER) {
		rigidBody->setGravity(btVector3(0, 10, 0));
		rigidBody->applyGravity();
		clock.restart();
	}

	sf::Mouse::setPosition(sf::Vector2i(Game::GetScreenWidth() / 2, Game::GetScreenHeight() / 2));
}

void Dalek::ToggleLook(bool enabled) {
	lookEnabled = enabled;
}

void Dalek::ToggleMovement(bool enabled) {
	movementEnabled = enabled;
}

void Dalek::ToggleFiring(bool enabled) {
	firingEnabled = enabled;
}